package com.prime.npd.util;

public class Constants {

	//Role
	public static final int ROLE_ADMIN = 1;
	public static final int ROLE_SALE = 2;
	
	public static final String ADMIN = "ADMIN";
	public static final String SALE = "SALE";
	
	public static final String DOT = ".";
	public static final String COMMA = ",";
	public static final String MINUS = "-";
	
	public static final String SINGLE_QUOTE = "'";
	public static final String EMPTY = "";
	public static final String SUCCESS = "SUCCESS";
	public static final String FAILED = "FAILED";
	
	//Province;
	public static final String LaoCai = "Tỉnh Lào Cai";
	public static final String DienBien = " Tỉnh Điện Biên";
	public static final String HaGiang = "Tỉnh Hà Giang";
	public static final String LaiChau = "Tỉnh Lai Châu";
	public static final String LangSon = "Tỉnh Lạng Sơn";
	public static final String SonLa = "Tỉnh Sơn La";
	public static final String YenBai = "Tỉnh Yên Bái";
	public static final String TuyenQuang = "Tỉnh Tuyên Quang";
	public static final String BacKan = "Tỉnh Bắc Kạn";
	public static final String CaoBang = "Tỉnh Cao Bằng";
	public static final String VinhPhuc = "Tỉnh Vĩnh Phúc";
	public static final String PhuTho = "Tỉnh Phú Thọ";
	public static final String ThaiNguyen = "Tỉnh Thái Nguyên";
	public static final String HaNoi = "TP Hà Nội";
	public static final String BacGiang = "Tỉnh Bắc Giang";
	public static final String BacNinh = "Tỉnh Bắc Ninh";
	public static final String HoaBinh = "Tỉnh Hòa Bình";
	public static final String HungYen = "Tỉnh Hưng Yên";
	public static final String HaiDuong = "Tỉnh Hải Dương";
	public static final String HaiPhong = "TP Hải Phòng";
	public static final String QuangNinh = "Tỉnh Quảng Ninh";
	public static final String HaNam = "Tỉnh Hà Nam";
	public static final String ThaiBinh = "Tỉnh Thái Bình";
	public static final String NamDinh = "Tỉnh Nam Định";
	public static final String NinhBinh = "Tỉnh Ninh Bình";
	public static final String ThanhHoa = "Tỉnh Thanh Hóa";
	public static final String NgheAn = "Tỉnh Nghệ An";
	public static final String HaTinh = "Tỉnh Hà Tĩnh";
	public static final String QuangBinh = "Tỉnh Quảng Bình";
	public static final String QuangTri = "Tỉnh Quảng Trị";
	public static final String ThuaThienHue = "Tỉnh Thừa Thiên Huế";
	public static final String DaNang = "TP Đà Nẵng";
	public static final String QuangNgai = "Tỉnh Quảng Ngãi";
	public static final String BinhDinh = "Tỉnh Bình Định";
	public static final String KhanhHoa = "Tỉnh Khánh Hòa";
	public static final String PhuYen = "Tỉnh Phú Yên";
	public static final String DacLak = "Tỉnh Đắc Lắk";
	public static final String GiaLai = "Tỉnh Gia Lai";
	public static final String KonTum = "Tỉnh Kon Tum";
	public static final String DakNong = "Tỉnh Đắk Nông";
	public static final String LamDong = "Tỉnh Lâm Đồng";
	public static final String NinhThuan = "Tỉnh Ninh Thuận";
	public static final String BinhThuan = "Tỉnh Bình Thuận";
	public static final String CanTho = "Tỉnh Cần Thơ";
	public static final String HCM = "TP Hồ Chí Minh";
	public static final String LongAn = "Tỉnh Long An";
	public static final String DongNai = "Tỉnh Đồng Nai";
	public static final String BenTre = "Tỉnh Bến Tre";
	public static final String TraVinh = "Tình Trà Vinh";
	public static final String DongThap = "Tỉnh Đồng Tháp";
	public static final String VungTau = "Tỉnh Bà Rịa, Vũng Tàu";
	public static final String TayNinh = "Tỉnh Tây Ninh";
	public static final String TienGiang = "Tỉnh Tiền Giang";
	public static final String BinhPhuoc = "Tỉnh Bình Phước";
	public static final String QuangNam = "Tỉnh Quảng Nam";
	
	public static final String URL_PRIME = "https://www.prime.vn/san-pham/gach/";
	
	//password
	public static final String PW_WRONG = "Mật khẩu cũ không đúng!";
	public static final String PW_NOTMATCH = "Xác nhận mật khẩu mới không trùng khớp!";
	public static final String PW_LENGTH = "Mật khẩu mới phải ít nhất 6 ký tự!";
	public static final String PW_SUCCESS = "Đổi mật khẩu thành công!";
	
	//region
	public static final String NORTH = "Miền Bắc";
	public static final String MIDDLE = "Miền Trung";
	public static final String SOUTH = "Miền Nam";
	public static final String AREA_01 = "Area 01";
	public static final String AREA_02 = "Area 02";
	public static final String AREA_03 = "Area 03";
	public static final String AREA_04 = "Area 04";
	public static final String AREA_05 = "Area 05";
	public static final String AREA_06 = "Area 06";
	public static final String AREA_07 = "Area 07";
	public static final String AREA_08 = "Area 08";
	public static final String AREA_09 = "Area 09";
	public static final String AREA_10 = "Area 10";
	public static final String AREA_11 = "Area 11";
	
}
