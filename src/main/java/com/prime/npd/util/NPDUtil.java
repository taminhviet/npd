package com.prime.npd.util;

import java.util.ArrayList;
import java.util.List;

public class NPDUtil {

	public static List<String> province(){
		List<String> province = new ArrayList<String>();
		province.add(Constants.LaoCai);
		province.add(Constants.DienBien);
		province.add(Constants.HaGiang);
		province.add(Constants.LaiChau);
		province.add(Constants.LangSon);
		province.add(Constants.SonLa);
		province.add(Constants.YenBai);
		province.add(Constants.TuyenQuang);
		province.add(Constants.BacKan);
		province.add(Constants.CaoBang);
		province.add(Constants.VinhPhuc);
		province.add(Constants.PhuTho);
		province.add(Constants.ThaiNguyen);
		province.add(Constants.HaNoi);
		province.add(Constants.BacGiang);
		province.add(Constants.BacNinh);
		province.add(Constants.HoaBinh);
		province.add(Constants.HungYen);
		province.add(Constants.HaiDuong);
		province.add(Constants.HaiPhong);
		province.add(Constants.QuangNinh);
		province.add(Constants.HaNam);
		province.add(Constants.ThaiBinh);
		province.add(Constants.NamDinh);
		province.add(Constants.NinhBinh);
		province.add(Constants.ThanhHoa);
		province.add(Constants.NgheAn);
		province.add(Constants.HaTinh);
		province.add(Constants.QuangBinh);
		province.add(Constants.QuangTri);
		province.add(Constants.ThuaThienHue);
		province.add(Constants.DaNang);
		province.add(Constants.QuangNgai);
		province.add(Constants.BinhDinh);
		province.add(Constants.KhanhHoa);
		province.add(Constants.PhuYen);
		province.add(Constants.DacLak);
		province.add(Constants.GiaLai);
		province.add(Constants.KonTum);
		province.add(Constants.DakNong);
		province.add(Constants.LamDong);
		province.add(Constants.NinhThuan);
		province.add(Constants.BinhThuan);
		province.add(Constants.CanTho);
		province.add(Constants.HCM);
		province.add(Constants.LongAn);
		province.add(Constants.DongNai);
		province.add(Constants.BenTre);
		province.add(Constants.TraVinh);
		province.add(Constants.DongThap);
		province.add(Constants.VungTau);
		province.add(Constants.TayNinh);
		province.add(Constants.TienGiang);
		province.add(Constants.BinhPhuoc);
		province.add(Constants.QuangNam);
		return province;
	}
}
