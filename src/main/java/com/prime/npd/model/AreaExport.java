package com.prime.npd.model;

public class AreaExport {

	private String area;
	private String factory;
	private String npdmonth;
	private String size;
	private String code;
	private int totalAllo;
	private int totalSell;
	private int totalPercent;
	private double saleMonth;
	private double saleYear;
	
	
	public AreaExport() {
		super();
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getFactory() {
		return factory;
	}
	public void setFactory(String factory) {
		this.factory = factory;
	}
	public String getNpdmonth() {
		return npdmonth;
	}
	public void setNpdmonth(String npdmonth) {
		this.npdmonth = npdmonth;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getTotalAllo() {
		return totalAllo;
	}
	public void setTotalAllo(int totalAllo) {
		this.totalAllo = totalAllo;
	}
	public int getTotalSell() {
		return totalSell;
	}
	public void setTotalSell(int totalSell) {
		this.totalSell = totalSell;
	}
	public int getTotalPercent() {
		return totalPercent;
	}
	public void setTotalPercent(int totalPercent) {
		this.totalPercent = totalPercent;
	}
	public double getSaleMonth() {
		return saleMonth;
	}
	public void setSaleMonth(double saleMonth) {
		this.saleMonth = saleMonth;
	}
	public double getSaleYear() {
		return saleYear;
	}
	public void setSaleYear(double saleYear) {
		this.saleYear = saleYear;
	}
	
}
