package com.prime.npd.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "User")
public class User {
    @Id
    @Column(name="username", nullable=false, unique=true)
    @Length(max=60, min=5)
    private String username;
    
    @Column(name="password", nullable=false, length=60)
    private String password;
    
    @Column(name="fullname")
    private String fullname;
    
    @Column(name="shortname")
    private String shortname;
    
    @Column(name="address")
    private String address;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "phone")
    private String phone;
    
    @Column(name = "isAdmin")
    private int isAdmin;
    

    public User() {
		super();
	}
    

	public User(@Length(max = 60, min = 5) String username, String password,
			String fullname, String shortname, String address, String email,
			String phone, int isAdmin) {
		super();
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.shortname = shortname;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.isAdmin = isAdmin;
	}



	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

	public int getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(int isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

    
}
