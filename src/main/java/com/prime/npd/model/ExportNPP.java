package com.prime.npd.model;

public class ExportNPP {
	private String name;
	private int allocation;
	private int sale;
	private int percent;
	private double mtd;
	private double ytd;
	
	public ExportNPP() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAllocation() {
		return allocation;
	}
	public void setAllocation(int allocation) {
		this.allocation = allocation;
	}
	public int getSale() {
		return sale;
	}
	public void setSale(int sale) {
		this.sale = sale;
	}
	public double getMtd() {
		return mtd;
	}
	public void setMtd(double mtd) {
		this.mtd = mtd;
	}
	public double getYtd() {
		return ytd;
	}
	public void setYtd(double ytd) {
		this.ytd = ytd;
	}
	public int getPercent() {
		return percent;
	}
	public void setPercent(int percent) {
		this.percent = percent;
	}
	
}
