package com.prime.npd.model;

public class SaleData {

	private String date;
	private String sct;
	private String name;
	private String factory;
	
	
	public SaleData() {
		super();
	}
	
	
	public SaleData(String date, String sct, String name, String factory) {
		super();
		this.date = date;
		this.sct = sct;
		this.name = name;
		this.factory = factory;
	}


	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getSct() {
		return sct;
	}
	public void setSct(String sct) {
		this.sct = sct;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFactory() {
		return factory;
	}
	public void setFactory(String factory) {
		this.factory = factory;
	}
	
}
