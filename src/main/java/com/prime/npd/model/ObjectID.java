package com.prime.npd.model;

public class ObjectID {
	private int id;
	
	public ObjectID(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
