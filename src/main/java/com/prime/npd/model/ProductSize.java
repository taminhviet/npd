package com.prime.npd.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product_size")
public class ProductSize {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name = "size")
	private String size;
	

	public ProductSize() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
	
	
}
