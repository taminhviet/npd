package com.prime.npd.model;

import java.util.Date;

public class SampleDisTable {

	 String sku;
	 String skuimage;
	 boolean allocated;
	 Date dateSample;
	 Date firstsale;
	 double mtd;
	 double ytd;
	
	public SampleDisTable() {
		super();
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	
	public Date getDateSample() {
		return dateSample;
	}
	public void setDateSample(Date dateSample) {
		this.dateSample = dateSample;
	}
	
	public Date getFirstsale() {
		return firstsale;
	}
	public void setFirstsale(Date firstsale) {
		this.firstsale = firstsale;
	}
	public double getMtd() {
		return mtd;
	}
	public void setMtd(double mtd) {
		this.mtd = mtd;
	}
	public double getYtd() {
		return ytd;
	}
	public void setYtd(double ytd) {
		this.ytd = ytd;
	}
	public boolean isAllocated() {
		return allocated;
	}
	public void setAllocated(boolean allocated) {
		this.allocated = allocated;
	}
	public String getSkuimage() {
		return skuimage;
	}
	public void setSkuimage(String skuimage) {
		this.skuimage = skuimage;
	}
	
	
}
