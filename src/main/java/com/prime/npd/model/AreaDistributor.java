package com.prime.npd.model;

import java.util.List;

public class AreaDistributor {

	String area;
	List<Distributor> distributors;
	
	
	public AreaDistributor() {
		super();
	}
	
	public AreaDistributor(String area, List<Distributor> distributors) {
		super();
		this.area = area;
		this.distributors = distributors;
	}

	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public List<Distributor> getDistributors() {
		return distributors;
	}
	public void setDistributors(List<Distributor> distributors) {
		this.distributors = distributors;
	}
	
	
}
