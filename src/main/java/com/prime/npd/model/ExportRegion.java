package com.prime.npd.model;

public class ExportRegion {
	private String region;
	private String factory;
	private String npdMonth;
	private String npdYear;
	private String size;
	private String code;
	private int totalNumAllo;
	private int totalNumSale;
	private int totalPercentSale;
	private double totalSale;
	private int mtdNumAllo;
	private int mtdNumSale;
	private int mtdPercentSale;
	private double mtdSale;
	private int ytdNumAllo;
	private int ytdNumSale;
	private int ytdPercentSale;
	private double ytdSale;
	
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getFactory() {
		return factory;
	}
	public void setFactory(String factory) {
		this.factory = factory;
	}
	public String getNpdMonth() {
		return npdMonth;
	}
	public void setNpdMonth(String npdMonth) {
		this.npdMonth = npdMonth;
	}
	public String getNpdYear() {
		return npdYear;
	}
	public void setNpdYear(String npdYear) {
		this.npdYear = npdYear;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getTotalNumAllo() {
		return totalNumAllo;
	}
	public void setTotalNumAllo(int totalNumAllo) {
		this.totalNumAllo = totalNumAllo;
	}
	public int getTotalNumSale() {
		return totalNumSale;
	}
	public void setTotalNumSale(int totalNumSale) {
		this.totalNumSale = totalNumSale;
	}
	public int getTotalPercentSale() {
		return totalPercentSale;
	}
	public void setTotalPercentSale(int totalPercentSale) {
		this.totalPercentSale = totalPercentSale;
	}
	public double getTotalSale() {
		return totalSale;
	}
	public void setTotalSale(double totalSale) {
		this.totalSale = totalSale;
	}
	public int getMtdNumAllo() {
		return mtdNumAllo;
	}
	public void setMtdNumAllo(int mtdNumAllo) {
		this.mtdNumAllo = mtdNumAllo;
	}
	public int getMtdNumSale() {
		return mtdNumSale;
	}
	public void setMtdNumSale(int mtdNumSale) {
		this.mtdNumSale = mtdNumSale;
	}
	public int getMtdPercentSale() {
		return mtdPercentSale;
	}
	public void setMtdPercentSale(int mtdPercentSale) {
		this.mtdPercentSale = mtdPercentSale;
	}
	public double getMtdSale() {
		return mtdSale;
	}
	public void setMtdSale(double mtdSale) {
		this.mtdSale = mtdSale;
	}
	public int getYtdNumAllo() {
		return ytdNumAllo;
	}
	public void setYtdNumAllo(int ytdNumAllo) {
		this.ytdNumAllo = ytdNumAllo;
	}
	public int getYtdNumSale() {
		return ytdNumSale;
	}
	public void setYtdNumSale(int ytdNumSale) {
		this.ytdNumSale = ytdNumSale;
	}
	public int getYtdPercentSale() {
		return ytdPercentSale;
	}
	public void setYtdPercentSale(int ytdPercentSale) {
		this.ytdPercentSale = ytdPercentSale;
	}
	public double getYtdSale() {
		return ytdSale;
	}
	public void setYtdSale(double ytdSale) {
		this.ytdSale = ytdSale;
	}
	
}
