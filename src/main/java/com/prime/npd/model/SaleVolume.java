package com.prime.npd.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sale_volumne")
public class SaleVolume {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
    Date requestdate;
	
	@Column(name = "orderno")
	private String orderno;
	
	@Column(name = "ponumber")
	private String ponumber;
	
	@Column(name = "fullname")
	private String fullname;
	
	@Column(name = "orderitem")
	private String orderitem;
	
	@Column(name = "quantities")
	private double quantities;
	
	@Column(name = "factory")
	private String factory;
	
	@Column(name = "size")
	private String size;
	
	@Column(name = "shortname")
	private String shortname;
	
	@Column(name = "squaremeter")
	private double squaremeter;
	
	@Column(name = "area")
	private String area;
	
	@Column(name = "month")
	private String month;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getRequestdate() {
		return requestdate;
	}

	public void setRequestdate(Date requestdate) {
		this.requestdate = requestdate;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getOrderitem() {
		return orderitem;
	}

	public void setOrderitem(String orderitem) {
		this.orderitem = orderitem;
	}

	public double getQuantities() {
		return quantities;
	}

	public void setQuantities(double quantities) {
		this.quantities = quantities;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public double getSquaremeter() {
		return squaremeter;
	}

	public void setSquaremeter(double squaremeter) {
		this.squaremeter = squaremeter;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public SaleVolume() {
		super();
	}

	public String getOrderno() {
		return orderno;
	}

	public void setOrderno(String orderno) {
		this.orderno = orderno;
	}

	public String getPonumber() {
		return ponumber;
	}

	public void setPonumber(String ponumber) {
		this.ponumber = ponumber;
	}
	
}
