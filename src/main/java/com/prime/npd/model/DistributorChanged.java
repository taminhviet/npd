package com.prime.npd.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DistributorChanged")
public class DistributorChanged {

	@Id
    @Column(name="id", nullable=false, unique=true)
    private String id;
	
	@Column(name = "currentfullname")
	private String currentfullname;
	
	@Column(name = "currentshortname")
	private String currentshortname;
	
	@Column(name = "oldfullname", length = 1000)
	private String oldfullname;
	
	@Column(name = "oldshortname", length = 1000)
	private String oldshortname;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCurrentfullname() {
		return currentfullname;
	}

	public void setCurrentfullname(String currentfullname) {
		this.currentfullname = currentfullname;
	}

	public String getCurrentshortname() {
		return currentshortname;
	}

	public void setCurrentshortname(String currentshortname) {
		this.currentshortname = currentshortname;
	}

	public String getOldfullname() {
		return oldfullname;
	}

	public void setOldfullname(String oldfullname) {
		this.oldfullname = oldfullname;
	}

	public String getOldshortname() {
		return oldshortname;
	}

	public void setOldshortname(String oldshortname) {
		this.oldshortname = oldshortname;
	}
	
	
}
