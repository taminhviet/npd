package com.prime.npd.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "authen_log")
public class AuthenLog {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
    Date date;
	
	@Column(name = "username")
	private String username;
	

	public AuthenLog() {
		super();
	}

	public AuthenLog(int id, Date date, String username) {
		super();
		this.id = id;
		this.date = date;
		this.username = username;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	
}
