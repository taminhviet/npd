package com.prime.npd.model;

public class ExportNPD {

	private String factory;
	private String size;
	private String code;
	private String monthNPD;
	private int totalAllocation;
	private int northAllocation;
	private int northSample;
	private int northActualAllocation;
	private int northActual;
	private int centralAllocation;
	private int centralSample;
	private int centralActual;
	private int centralActualAllocation;
	private int southAllocation;
	private int southSample;
	private int southActual;
	private int southActualAllocation;
	private double ytd;
	
	public ExportNPD() {
		super();
	}
	public String getFactory() {
		return factory;
	}
	public void setFactory(String factory) {
		this.factory = factory;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMonthNPD() {
		return monthNPD;
	}
	public void setMonthNPD(String monthNPD) {
		this.monthNPD = monthNPD;
	}
	public double getYtd() {
		return ytd;
	}
	public void setYtd(double ytd) {
		this.ytd = ytd;
	}
	public int getTotalAllocation() {
		return totalAllocation;
	}
	
	public int getNorthActualAllocation() {
		return northActualAllocation;
	}
	public void setNorthActualAllocation(int northActualAllocation) {
		this.northActualAllocation = northActualAllocation;
	}
	public void setTotalAllocation(int totalAllocation) {
		this.totalAllocation = totalAllocation;
	}
	public int getNorthAllocation() {
		return northAllocation;
	}
	public void setNorthAllocation(int northAllocation) {
		this.northAllocation = northAllocation;
	}
	public int getNorthSample() {
		return northSample;
	}
	public void setNorthSample(int northSample) {
		this.northSample = northSample;
	}
	public int getNorthActual() {
		return northActual;
	}
	public void setNorthActual(int northActual) {
		this.northActual = northActual;
	}
	public int getCentralAllocation() {
		return centralAllocation;
	}
	public void setCentralAllocation(int centralAllocation) {
		this.centralAllocation = centralAllocation;
	}
	public int getCentralSample() {
		return centralSample;
	}
	public void setCentralSample(int centralSample) {
		this.centralSample = centralSample;
	}
	public int getCentralActual() {
		return centralActual;
	}
	public void setCentralActual(int centralActual) {
		this.centralActual = centralActual;
	}
	public int getCentralActualAllocation() {
		return centralActualAllocation;
	}
	public void setCentralActualAllocation(int centralActualAllocation) {
		this.centralActualAllocation = centralActualAllocation;
	}
	public int getSouthAllocation() {
		return southAllocation;
	}
	public void setSouthAllocation(int southAllocation) {
		this.southAllocation = southAllocation;
	}
	public int getSouthSample() {
		return southSample;
	}
	public void setSouthSample(int southSample) {
		this.southSample = southSample;
	}
	public int getSouthActual() {
		return southActual;
	}
	public void setSouthActual(int southActual) {
		this.southActual = southActual;
	}
	public int getSouthActualAllocation() {
		return southActualAllocation;
	}
	public void setSouthActualAllocation(int southActualAllocation) {
		this.southActualAllocation = southActualAllocation;
	}
	
	
}
