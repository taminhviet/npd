package com.prime.npd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.npd.model.ProductSize;
import com.prime.npd.repository.ProductSizeRepository;

@Service
public class ProductSizeService {

	@Autowired
	ProductSizeRepository productSizeRepository;
	
	public void saveProductSize(ProductSize productSize){
		productSizeRepository.save(productSize);
	}
	
	public ProductSize findBySize(String size){
		return productSizeRepository.findBySize(size);
	}
	
	public List<ProductSize> findAll(){
		return productSizeRepository.findAll();
	}
}
