package com.prime.npd.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.prime.npd.model.SaleVolume;
import com.prime.npd.repository.SaleVolumeRepository;

@Service
public class SaleVolumeService {

	@Autowired
	SaleVolumeRepository saleVolumeRepository;
	
	public void saveSaleVolume(SaleVolume saleVolume){
		saleVolumeRepository.save(saleVolume);
	}
	
	public List<SaleVolume> findAll(){
		return saleVolumeRepository.findAll();
	}
	public List<SaleVolume> findTop2k(){
		Pageable page2000 = PageRequest.of(0, 2000,Sort.by("id").descending());
		return saleVolumeRepository.findAll(page2000).getContent();
	}
	public List<SaleVolume> findByOrderitem(String orderitem){
		return saleVolumeRepository.findByOrderitem(orderitem);
	}
	public List<SaleVolume> findSaleVolumeWithYear(String code, String year){
		return saleVolumeRepository.findSaleVolumeWithYear(code, year);
	}
	public List<SaleVolume> findSaleVolumeWithArea(String code, String area){
		return saleVolumeRepository.searchByArea(code, area);
	}
	public List<SaleVolume> findByFullname(String fullname){
		return saleVolumeRepository.findByFullname(fullname);
	}
	public List<SaleVolume> findByShortname(String shortname){
		return saleVolumeRepository.findByShortname(shortname);
	}
	
	public List<SaleVolume> findByDis(String orderitem, String fullname){
		return saleVolumeRepository.searchByDis(orderitem, fullname);
	}
	public List<SaleVolume> findSaleVolumeWithNPPYear(String fullname, String year){
		return saleVolumeRepository.findSaleVolumeWithNPPYear(fullname, year);
	}
	public List<SaleVolume> findSaleVolumeWithSNNPPYear(String shortname, String year){
		return saleVolumeRepository.findSaleVolumeWithSNNPPYear(shortname, year);
	}
	public List<SaleVolume> findByDisDate(String orderitem, String fullname, Date requestdate, String orderno){
		return saleVolumeRepository.searchByDisDate(orderitem, fullname, requestdate, orderno);
	}
	
	public void deleteAll(){
		saleVolumeRepository.deleteAll();
	}
}
