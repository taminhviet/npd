package com.prime.npd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.npd.model.AuthenLog;
import com.prime.npd.repository.AuthenLogRepository;

@Service
public class AuthenLogService {

	@Autowired
	AuthenLogRepository authenLogRepository;

	public List<AuthenLog> getAll(){
		return authenLogRepository.findAll();
	}
	public List<AuthenLog> findbyuser(String username){
		return authenLogRepository.findByUsername(username);
	}
	
	public void saveLog(AuthenLog authenLog) {
		authenLogRepository.save(authenLog);
	}
}
