package com.prime.npd.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prime.npd.model.User;
import com.prime.npd.util.Constants;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	UserService userService;
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		User user = userService.findbyUsername(username);
		UserDetails userDetail = null;
		try {
			Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
			if(Constants.ROLE_ADMIN == user.getIsAdmin()){
				grantedAuthorities.add(new SimpleGrantedAuthority(Constants.ADMIN));
			}else {
				grantedAuthorities.add(new SimpleGrantedAuthority(Constants.SALE));
			}
			userDetail =  new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDetail;
	}

}
