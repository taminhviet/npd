package com.prime.npd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.npd.model.Product;
import com.prime.npd.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;
	
	public void saveProduct(Product product){
		productRepository.save(product);
	}
	public List<Product> getAllProducts(){
		return productRepository.findAll();
	}
	public Product findByCode(String code){
		return productRepository.findByCode(code);
	}
	public List<Product> findBySize(String size){
		return productRepository.findBySize(size);
	}
	public List<Product> findByFactory(String factory){
		return productRepository.findByFactory(factory);
	}
	
	public void deleteAllproduct(){
		productRepository.deleteAll();
	}
}
