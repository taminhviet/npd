package com.prime.npd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.prime.npd.model.Sample;
import com.prime.npd.repository.SampleRepository;

@Service
public class SampleService {

	@Autowired
	SampleRepository sampleRepository;
	
	public void saveSample(Sample sample){
		sampleRepository.save(sample);
	}
	
	public List<Sample> findAllSample(){
		return sampleRepository.findAll();
	}
	public List<Sample> find2kSample(){
		Pageable page2000 = PageRequest.of(0, 2000,Sort.by("id").descending());
		return sampleRepository.findAll(page2000).getContent();
	}
	public void deleteAllSample(){
		sampleRepository.deleteAll();
	}
	
	public List<Sample> findByCustomer(String customer){
		return sampleRepository.findByCustomer(customer);
	}
	public List<Sample> findByItem(String item){
		return sampleRepository.findByItem(item);
	}
	
	public List<Sample> findByItemYear(String code, String year){
		return sampleRepository.findSampleWithYear(code, year);
	}
	
	public List<Sample> searchByCI(String customer, String item){
		return sampleRepository.searchByCI(customer, item);
	}
}
