package com.prime.npd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.npd.model.Area;
import com.prime.npd.repository.AreaRepository;

@Service
public class AreaService {
	
	@Autowired
	AreaRepository areaRepository;
	
	public List<Area> findAllArea(){
		return areaRepository.findAll();
	}
	
	public Area findByName(String name){
		return areaRepository.findByName(name);
	}
	
	public void saveArea(Area area){
		areaRepository.save(area);
	}
	
	public void deleteArea(){
		areaRepository.deleteAll();
	}
}
