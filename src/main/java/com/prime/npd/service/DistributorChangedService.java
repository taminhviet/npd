package com.prime.npd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.npd.model.DistributorChanged;
import com.prime.npd.repository.DistributorChangedRepository;

@Service
public class DistributorChangedService {

	@Autowired
	DistributorChangedRepository distributorChangedRepository;
	
	public DistributorChanged findById(String id) {
		return distributorChangedRepository.findById(id);
	}
	
	public void saveDistributorChanged(DistributorChanged distributorChanged) {
		distributorChangedRepository.save(distributorChanged);
	}
}
