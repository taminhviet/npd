package com.prime.npd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.prime.npd.model.ProductAllocation;
import com.prime.npd.repository.ProductAllocationRepository;

@Service
public class ProductAllocationService {

	@Autowired
	ProductAllocationRepository productAllocationRepository;
	
	public void saveProductAllocation(ProductAllocation productAllocation){
		productAllocationRepository.save(productAllocation);
	}
	
	public List<ProductAllocation> findByShortname(String shortname){
		return productAllocationRepository.findByShortname(shortname);
	}
	
	public List<ProductAllocation> findByMonth(String month){
		return productAllocationRepository.findByMonth(month);
	}
	public List<ProductAllocation> findByCode(String code){
		return productAllocationRepository.findByCode(code);
	}
	public List<ProductAllocation> findByYear(String code,String year){
		return productAllocationRepository.findAllocationWithYear(code,year);
	}
	
	public ProductAllocation findById(int id) {
		return productAllocationRepository.findById(id).get();
	}
	
	public ProductAllocation searchByDis(String code, String taxid){
		List<ProductAllocation> lsAllocations = productAllocationRepository.searchByDis(code, taxid);
		if(lsAllocations != null && !lsAllocations.isEmpty()){
			return lsAllocations.get(0);
		}else {
			return null;
		}
	}
	
	public List<ProductAllocation> searchByMonthName(String shortname, String month){
		return productAllocationRepository.searchByMonthName(shortname, month);
	}
	public List<ProductAllocation> searchByYearName(String shortname, String year){
		return productAllocationRepository.searchByYearName(shortname, year);
	}
	
	public List<ProductAllocation> getAll(){
		return productAllocationRepository.findAll();
	}
	public List<ProductAllocation> get5000Allo(){
		Pageable page5000 = PageRequest.of(0, 5000,Sort.by("id").descending());
		return productAllocationRepository.findAll(page5000).getContent();
	}
	
	public void deletePA(ProductAllocation pa) {
		productAllocationRepository.delete(pa);
	}
	
	public void deleteAll(){
		productAllocationRepository.deleteAll();
	}
}
