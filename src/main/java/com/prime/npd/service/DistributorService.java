package com.prime.npd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.npd.model.Distributor;
import com.prime.npd.repository.DistributorRepository;

@Service
public class DistributorService {

	@Autowired
	DistributorRepository distributorRepository;
	
	public void saveDistributor(Distributor distributor){
		distributorRepository.save(distributor);
	}
	
	public List<Distributor> findBySale(String sale){
		return distributorRepository.findBySale(sale);
	}
	public Distributor findById(String id){
		return distributorRepository.findById(id);
	}
	public Distributor findByShortname(String shortname) {
		return distributorRepository.findByShortname(shortname);
	}
	public Distributor findByFullname(String fullname) {
		return distributorRepository.findByFullname(fullname);
	}
	
	public List<Distributor> findAll(){
		return distributorRepository.findAll();
	}
	public List<Distributor> findByArea(String area){
		return distributorRepository.findByArea(area);
	}
	
	public void deleteAllDistributor(){
		distributorRepository.deleteAll();
	}
}
