package com.prime.npd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prime.npd.model.FirstSale;
import com.prime.npd.repository.FirstSaleRepository;

@Service
public class FirstSaleService {
	
	@Autowired 
	FirstSaleRepository firstSaleRepository;
	
	public FirstSale findByCode(String code) {
		return firstSaleRepository.findByCode(code);
	}
	
	public void saveFirstSale (FirstSale firstSale) {
		firstSaleRepository.save(firstSale);
	}

}
