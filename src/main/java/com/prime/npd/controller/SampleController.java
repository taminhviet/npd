package com.prime.npd.controller;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.prime.npd.model.Sample;
import com.prime.npd.service.SampleService;

@Controller
public class SampleController {

	@Autowired
	SampleService sampleService;
	
	@RequestMapping(value = "/admin/sample", method = RequestMethod.GET)
	public String sample(Model model) {
		List<Sample> samples = sampleService.find2kSample();
		if(samples != null && !samples.isEmpty()){
			model.addAttribute("samples", samples);
		}
		return "sample";
	}
	@RequestMapping(value = "/admin/deleteallsample", method = RequestMethod.GET)
	public String deleteallsample(Model model) {
		sampleService.deleteAllSample();
		return "redirect:/admin/sample";
	}
	@RequestMapping(value = "/admin/uploadsample", method = RequestMethod.POST)
	public String uploadsample(@RequestParam("file") MultipartFile file,
			HttpServletRequest request) {
		List<Sample> samples = new ArrayList<Sample>();
		try {
			String nameFile = file.getOriginalFilename();
			InputStream excelFile = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				int rowindex = currentRow.getRowNum();
				if(rowindex == 0){
					continue;
				}
				Sample sample = new Sample();
				Iterator<Cell> cellIterator = currentRow.iterator();
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int colindex = currentCell.getColumnIndex();
					switch (colindex) {
					case 0:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							sample.setOrderno(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf((int)currentCell.getNumericCellValue());
							sample.setOrderno(cellValue);
						}
						break;
					case 1:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							sample.setInvoice(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf((int)currentCell.getNumericCellValue());
							sample.setInvoice(cellValue);
						}
						break;
						
					case 2:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							Date date = df.parse(cellValue);
							sample.setDate(date);
						}
						break;
					case 3:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							sample.setCustomer(cellValue);
						}
						break;
					case 4:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							sample.setItem(cellValue);
						}
						break;
					case 5:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							int cellValue = Integer.valueOf(currentCell.getStringCellValue());
							sample.setQuantity(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							double cellValue = currentCell.getNumericCellValue();
							sample.setQuantity((int) cellValue);
						}
						break;
					case 6:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							int cellValue = Integer.valueOf(currentCell.getStringCellValue());
							sample.setGrade(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							double cellValue = currentCell.getNumericCellValue();
							sample.setGrade((int) cellValue);
						}
						break;
					default:
						break;
					}
				}
				samples.add(sample);
			}
			if(!samples.isEmpty()){
				for (Sample sample : samples) {
					List<Sample> existSample = sampleService.searchByCI(sample.getCustomer(), sample.getItem());
					if(existSample == null || existSample.isEmpty()){
						sampleService.saveSample(sample);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/sample";
	}
}
