package com.prime.npd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prime.npd.model.Area;
import com.prime.npd.model.Distributor;
import com.prime.npd.model.User;
import com.prime.npd.service.AreaService;
import com.prime.npd.service.DistributorService;
import com.prime.npd.service.UserService;
import com.prime.npd.util.NPDUtil;

@Controller
public class AdminController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	DistributorService distributorService;
	
	@Autowired
	AreaService areaService;
	

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String admin(Model model) {
		List<Distributor> lsDistributor = distributorService.findAll();
		if(lsDistributor != null && !lsDistributor.isEmpty()){
			model.addAttribute("lsDistributor", lsDistributor);
		}
		
		List<String> lsProvince = NPDUtil.province();
		model.addAttribute("lsProvince", lsProvince);
		
		List<Area> lsArea = areaService.findAllArea();
		model.addAttribute("lsArea", lsArea);
		
		List<User> lsSale = userService.findSale();
		model.addAttribute("lsSale", lsSale);
		
		return "admin";
	}
	

	
	
}
