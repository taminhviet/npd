package com.prime.npd.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.prime.npd.model.AreaExport;
import com.prime.npd.model.Distributor;
import com.prime.npd.model.ExportNPD;
import com.prime.npd.model.ExportNPP;
import com.prime.npd.model.ExportRegion;
import com.prime.npd.model.Factory;
import com.prime.npd.model.Product;
import com.prime.npd.model.ProductAllocation;
import com.prime.npd.model.SaleVolume;
import com.prime.npd.model.Sample;
import com.prime.npd.service.DistributorService;
import com.prime.npd.service.FactoryService;
import com.prime.npd.service.ProductAllocationService;
import com.prime.npd.service.ProductService;
import com.prime.npd.service.SaleVolumeService;
import com.prime.npd.service.SampleService;
import com.prime.npd.util.Constants;

@Controller
public class ReportController {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	ProductAllocationService productAllocationService;
	
	@Autowired
	DistributorService distributorService;
	
	@Autowired
	FactoryService factoryService;
	
	@Autowired
	SampleService sampleService;
	
	@Autowired
	SaleVolumeService saleVolumeService;

	@RequestMapping(value = "/admin/report", method = RequestMethod.GET)
	public String report(Model model) {
		List<String> years = getYears();
		List<String> months = getMonths();
		model.addAttribute("years", years);
		model.addAttribute("months", months);
		return "report";
	}
	
	@RequestMapping(value = "/admin/exportnpp", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> exportNPP(Model model) throws IOException{
		List<Distributor> lsDistributor = distributorService.findAll();
		List<ExportNPP> lsNPP = new ArrayList<ExportNPP>();
		String year = String.valueOf(Year.now().getValue());
		for (Distributor distributor : lsDistributor) {
			ExportNPP exportNPP = new ExportNPP();
			List<String> lsPA = new ArrayList<String>();
			double ytd = 0;
			Set<String> setItem = new HashSet<String>();
			//get allocation number
			String shortname = distributor.getShortname();
			List<ProductAllocation> lsAllocation = productAllocationService.findByShortname(shortname);
			if(lsAllocation != null && !lsAllocation.isEmpty()) {
				int i = 0;
				for (ProductAllocation allocation : lsAllocation) {
					if(allocation.getMonth().contains(year)){
						lsPA.add(allocation.getCode());
						i+=1;
					}
				}
				exportNPP.setAllocation(i);
			}
			//get sale total
			String fullname = distributor.getFullname();
			exportNPP.setName(fullname);
			List<SaleVolume> lsSaleVolumes = saleVolumeService.findSaleVolumeWithSNNPPYear(shortname,year);
			if(lsSaleVolumes != null && !lsSaleVolumes.isEmpty()) {
				for (SaleVolume sv : lsSaleVolumes) {
					String item = sv.getOrderitem();
					double m2 = sv.getSquaremeter();
					if(m2 >= 10 && lsPA.contains(item)) {
						ytd += m2;
						setItem.add(item);
					}
				}
				exportNPP.setSale(setItem.size());
				exportNPP.setMtd(0);
				exportNPP.setYtd(ytd);
			}
			int percentTotal = 0;
			if(exportNPP.getAllocation() > 0) {
				percentTotal = exportNPP.getSale() * 100 / exportNPP.getAllocation();
			}
			exportNPP.setPercent(percentTotal);
			lsNPP.add(exportNPP);
		}
		ByteArrayInputStream in = nppToExcel(lsNPP);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=ReportNPP.xlsx");
		return ResponseEntity.ok().headers(headers)
				.body(new InputStreamResource(in));
	}
	
	
	@RequestMapping(value = "/admin/exportdataarea", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> exportArea(Model model) throws IOException {
		List<Product> products = productService.getAllProducts();
		List<AreaExport> lsAreaExport = new ArrayList<AreaExport>();
		for (Product product : products) {
			
			String code = product.getCode();
			String npdMonth = product.getMonthnpd();
			String npdSize = product.getSize();
			// get factory
			String factCode = code.substring(0, 2);
			Factory factory = factoryService.findById(factCode);
			String factoryName = factory.getName();
			
			//get product allocation
			List<ProductAllocation> listPA = productAllocationService.findByCode(code);
			
			Map<String, Integer> mapCodearea = new HashMap<>();
			if(listPA != null && !listPA.isEmpty()) {
				for (ProductAllocation pa : listPA) {
					String shortname = pa.getShortname();
					Distributor distributor = distributorService.findByShortname(shortname);
					if(distributor != null) {
						String area = distributor.getArea();
						String codearea = code.concat(Constants.COMMA).concat(area);
						//+1 number of codearea
						if(mapCodearea.containsKey(codearea)) {
							mapCodearea.computeIfPresent(codearea, (k, v) -> v + 1);
						}else {
							mapCodearea.put(codearea, 1);
						}
					}
				}
				for (Map.Entry<String, Integer> entryCodeArea : mapCodearea.entrySet()) {
					AreaExport areaExport = new AreaExport();
					areaExport.setSize(npdSize);
					areaExport.setCode(code);
					areaExport.setNpdmonth(npdMonth);
					areaExport.setFactory(factoryName);
					String key = entryCodeArea.getKey();
					int valuePA = entryCodeArea.getValue();
					String[] areaArray = key.split(Constants.COMMA);
					String area = areaArray[1];
					areaExport.setArea(area);
					areaExport.setTotalAllo(valuePA);
					double totalSale = 0;
					int numSale = 0;
					List<SaleVolume> listSaleVolume = saleVolumeService.findSaleVolumeWithArea(code,area);
					List<SaleVolume> listSaleVolumeDis = new ArrayList<SaleVolume>();
					if(listSaleVolume != null && !listSaleVolume.isEmpty()) {
						for (SaleVolume sv : listSaleVolume) {
							double sqmt = sv.getSquaremeter();
							if(sqmt >= 10) {
								totalSale += sqmt; 
								if(!containsName(listSaleVolumeDis, sv.getFullname())) {
									listSaleVolumeDis.add(sv);
								}
							}
						}
					}
					numSale = listSaleVolumeDis.size();
					areaExport.setTotalSell(numSale);
					
					int percentTotal = 0;
					percentTotal = numSale * 100 / valuePA;
					areaExport.setSaleYear(totalSale);
					areaExport.setTotalPercent(percentTotal);
					lsAreaExport.add(areaExport);
			    }
			}
//			break;
		}
		ByteArrayInputStream in = areaToExcel(lsAreaExport);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=ReportArea.xlsx");
		return ResponseEntity.ok().headers(headers)
				.body(new InputStreamResource(in));
	}
	
	@RequestMapping(value = "/admin/exportbyregion", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> exportRegion(Model model,
			@RequestParam(name = "month", required= false) String month,
			@RequestParam(name = "year", required= false) String year) throws IOException {
		List<Product> products = productService.getAllProducts();
		List<ExportRegion> lsExportRegion = new ArrayList<ExportRegion>();
		Calendar ca  = Calendar.getInstance();
		String nmonth = String.valueOf(ca.get(Calendar.MONTH)+1);
		if(!month.contains(Constants.MINUS)) {
			nmonth = month;
		}
		String yearNow = year;
		for (Product product : products) {
			String code = product.getCode();
			String npdMonth = product.getMonthnpd();
			String npdSize = product.getSize();
			String npdYear = npdMonth.substring(0, 4);
			// get factory
			String factCode = code.substring(0, 2);
			Factory factory = factoryService.findById(factCode);
			String factoryName = factory.getName();
			//init North
			ExportRegion northRegion = new ExportRegion();
			northRegion.setRegion(Constants.NORTH);
			northRegion.setFactory(factoryName);
			northRegion.setNpdMonth(npdMonth);
			northRegion.setNpdYear(npdYear);
			northRegion.setSize(npdSize);
			northRegion.setCode(code);
			int northAllocation = 0;
			int northSaleVolume = 0;
			double northTotalSale = 0;
			int northYtdAllocation = 0;
			int northYtdSaleVolumn = 0;
			double northYtdSale = 0;
			int northMtdAllocation = 0;
			int northMtdSaleVolumn = 0;
			double northMtdSale = 0;
			
			//init Central
			ExportRegion centralRegion = new ExportRegion();
			centralRegion.setRegion(Constants.MIDDLE);
			centralRegion.setFactory(factoryName);
			centralRegion.setNpdMonth(npdMonth);
			centralRegion.setNpdYear(npdYear);
			centralRegion.setSize(npdSize);
			centralRegion.setCode(code);
			int centralAllocation = 0;
			int centralSaleVolume = 0;
			double centralTotalSale = 0;
			int centralYtdAllocation = 0;
			int centralYtdSaleVolumn = 0;
			double centralYtdSale = 0;
			int centralMtdAllocation = 0;
			int centralMtdSaleVolumn = 0;
			double centralMtdSale = 0;
			
			//init South
			ExportRegion southRegion = new ExportRegion();
			southRegion.setRegion(Constants.SOUTH);
			southRegion.setFactory(factoryName);
			southRegion.setNpdMonth(npdMonth);
			southRegion.setNpdYear(npdYear);
			southRegion.setSize(npdSize);
			southRegion.setCode(code);
			int southAllocation = 0;
			int southSaleVolume = 0;
			double southTotalSale = 0;
			int southYtdAllocation = 0;
			int southYtdSaleVolumn = 0;
			double southYtdSale = 0;
			int southMtdAllocation = 0;
			int southMtdSaleVolumn = 0;
			double southMtdSale = 0;
			
			//get product allocation
			List<ProductAllocation> listPA = productAllocationService.findByCode(code);
			if(listPA != null && !listPA.isEmpty()) {
				for (ProductAllocation pa : listPA) {
					String shortname = pa.getShortname();
					String paMonth = pa.getMonth();
					Distributor distributor = distributorService.findByShortname(shortname);
					if(distributor != null) {
						String area = distributor.getArea(); 
						String region = checkRegion(area);
						switch (region) {
						case Constants.NORTH:
							northAllocation += 1;
							if(paMonth.contains(yearNow)) {
								northYtdAllocation +=1;
							}
							if(paMonth.contains(nmonth)) {
								northMtdAllocation +=1;
							}
							break;
						case Constants.MIDDLE:
							centralAllocation += 1;
							if(paMonth.contains(yearNow)) {
								centralYtdAllocation +=1;
							}
							if(paMonth.contains(nmonth)) {
								centralMtdAllocation +=1;
							}
							break;
						case Constants.SOUTH:
							southAllocation += 1;
							if(paMonth.contains(yearNow)) {
								southYtdAllocation +=1;
							}
							if(paMonth.contains(nmonth)) {
								southMtdAllocation +=1;
							}
							break;
						default:
							break;
						}
					}
//					else {
//						System.out.println(shortname);
//					}
				}
			}
			
			
			// get product sale
			List<SaleVolume> listSaleVolume = saleVolumeService.findByOrderitem(code);
			Set<String> fullnameSaleVolume = new HashSet<String>();
			for (SaleVolume saleVolume : listSaleVolume) {
				String fullname = saleVolume.getFullname();
				String monthSaleVolume = saleVolume.getMonth();
				double sqmt = saleVolume.getSquaremeter();
				Distributor distributor = distributorService.findByFullname(fullname);
				if(distributor != null && sqmt >= 10) {
					String area = distributor.getArea();
					String region = checkRegion(area);
					//add sale total
					switch (region) {
					case Constants.NORTH:
						northTotalSale += sqmt;
						if(monthSaleVolume.contains(yearNow)) {
							northYtdSale += sqmt;
						}
						if(monthSaleVolume.contains(nmonth)) {
							northMtdSale += sqmt;
						}
						break;
					case Constants.MIDDLE:
						centralTotalSale += sqmt;
						if(monthSaleVolume.contains(yearNow)) {
							centralYtdSale += sqmt;
						}
						if(monthSaleVolume.contains(nmonth)) {
							centralMtdSale += sqmt;
						}
						break;
					case Constants.SOUTH:
						southTotalSale += sqmt;
						if(monthSaleVolume.contains(yearNow)) {
							southYtdSale += sqmt;
						}
						if(monthSaleVolume.contains(nmonth)) {
							southMtdSale += sqmt;
						}
						break;
					default:
						break;
					}
					//count number of sale
					if(!fullnameSaleVolume.contains(fullname)) {
						switch (region) {
						case Constants.NORTH:
							northSaleVolume += 1;
							if(monthSaleVolume.contains(yearNow)) {
								northYtdSaleVolumn += 1;
							}
							if(monthSaleVolume.contains(nmonth)) {
								northMtdSaleVolumn += 1;
							}
							break;
						case Constants.MIDDLE:
							centralSaleVolume += 1;
							if(monthSaleVolume.contains(yearNow)) {
								centralYtdSaleVolumn += 1;
							}
							if(monthSaleVolume.contains(nmonth)) {
								centralMtdSaleVolumn += 1;
							}
							break;
						case Constants.SOUTH:
							southSaleVolume += 1;
							if(monthSaleVolume.contains(yearNow)) {
								southYtdSaleVolumn += 1;
							}
							if(monthSaleVolume.contains(nmonth)) {
								southMtdSaleVolumn += 1;
							}
							break;
						default:
							break;
						}
						fullnameSaleVolume.add(fullname);
					}
				}
			}
			//set total allo
			northRegion.setTotalNumAllo(northAllocation);
			centralRegion.setTotalNumAllo(centralAllocation);
			southRegion.setTotalNumAllo(southAllocation);
			
			//set total sale
			northRegion.setTotalNumSale(northSaleVolume);
			centralRegion.setTotalNumSale(centralSaleVolume);
			southRegion.setTotalNumSale(southSaleVolume);
			
			//set percent
			int northtotalPercentSale = 0;
			if(northAllocation > 0) {
				northtotalPercentSale = northSaleVolume * 100 / northAllocation;
			}
			northRegion.setTotalPercentSale(northtotalPercentSale);
			
			int centraltotalPercentSale = 0;
			if(centralAllocation > 0) {
				centraltotalPercentSale = centralSaleVolume * 100 / centralAllocation;
			}
			centralRegion.setTotalPercentSale(centraltotalPercentSale);
			
			int southtotalPercentSale = 0;
			if(southAllocation > 0) {
				southtotalPercentSale = southSaleVolume * 100 / southAllocation;
			}
			southRegion.setTotalPercentSale(southtotalPercentSale);
			
			//set total sale
			northRegion.setTotalSale(northTotalSale);
			centralRegion.setTotalSale(centralTotalSale);
			southRegion.setTotalSale(southTotalSale);
			
			//set ytd allo
			northRegion.setYtdNumAllo(northYtdAllocation);
			centralRegion.setYtdNumAllo(centralYtdAllocation);
			southRegion.setYtdNumAllo(southYtdAllocation);
			
			//set ytd num sale
			northRegion.setYtdNumSale(northYtdSaleVolumn);
			centralRegion.setYtdNumSale(centralYtdSaleVolumn);
			southRegion.setYtdNumSale(southYtdSaleVolumn);
			
			//set percent ytd
			int northYtdPercentSale = 0;
			if(northYtdAllocation > 0) {
				northYtdPercentSale = northYtdSaleVolumn * 100 / northYtdAllocation;
			}
			northRegion.setYtdPercentSale(northYtdPercentSale);
			
			int centralYtdPercentSale = 0;
			if(centralYtdPercentSale > 0) {
				centralYtdPercentSale = centralYtdSaleVolumn * 100 / centralYtdAllocation;
			}
			centralRegion.setYtdPercentSale(centralYtdPercentSale);
			
			int southYtdPercentSale = 0;
			if(southYtdAllocation > 0) {
				southYtdPercentSale = southYtdSaleVolumn * 100 / southYtdAllocation;
			}
			southRegion.setYtdPercentSale(southYtdPercentSale);
			
			//set total sale
			northRegion.setYtdSale(northYtdSale);
			centralRegion.setYtdSale(centralYtdSale);
			southRegion.setYtdSale(southYtdSale);
			
			//set mtd allo
			northRegion.setMtdNumAllo(northMtdAllocation);
			centralRegion.setMtdNumAllo(centralMtdAllocation);
			southRegion.setMtdNumAllo(southMtdAllocation);
			
			//set mtd num sale
			northRegion.setMtdNumSale(northMtdSaleVolumn);
			centralRegion.setMtdNumSale(centralMtdSaleVolumn);
			southRegion.setMtdNumSale(southMtdSaleVolumn);
			
			//set percent mtd
			int northMtdPercentSale = 0;
			if(northMtdAllocation > 0) {
				northMtdPercentSale = northMtdSaleVolumn * 100 / northMtdAllocation;
			}
			northRegion.setMtdPercentSale(northMtdPercentSale);
			
			int centralMtdPercentSale = 0;
			if(centralMtdPercentSale > 0) {
				centralMtdPercentSale = centralMtdSaleVolumn * 100 / centralMtdAllocation;
			}
			centralRegion.setMtdPercentSale(centralMtdPercentSale);
			
			int southMtdPercentSale = 0;
			if(southMtdAllocation > 0) {
				southMtdPercentSale = southMtdSaleVolumn * 100 / southMtdAllocation;
			}
			southRegion.setMtdPercentSale(southMtdPercentSale);
			
			//set total sale
			northRegion.setMtdSale(northMtdSale);
			centralRegion.setMtdSale(centralMtdSale);
			southRegion.setMtdSale(southMtdSale);
			
			lsExportRegion.add(northRegion);
			lsExportRegion.add(centralRegion);
			lsExportRegion.add(southRegion);
		}
		ByteArrayInputStream in = regionToExcel(lsExportRegion);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=ReportRegion.xlsx");
		return ResponseEntity.ok().headers(headers)
				.body(new InputStreamResource(in));
	}
	
	
	@RequestMapping(value = "/admin/exportdata", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> export(Model model) throws IOException {
		Calendar ca  = Calendar.getInstance();
		List<Product> products = productService.getAllProducts();
		List<ExportNPD> exportNPDs = new ArrayList<ExportNPD>();
		for (Product product : products) {
			int northAllocation = 0;
			int centralAllocation = 0;
			int southAllocation = 0;
			int northSample = 0;
			int centralSample = 0;
			int southSample = 0;
			int northSaleVolume = 0;
			int centralSaleVolume = 0;
			int southSaleVolume = 0;
			double ytd = 0;
			ExportNPD exportNpd = new ExportNPD();
			String code = product.getCode();
			exportNpd.setCode(code);
			exportNpd.setSize(product.getSize());
			exportNpd.setMonthNPD(product.getMonthnpd());
			String factCode = code.substring(0, 2);
			Factory factory = factoryService.findById(factCode);
			String factoryName = factory.getName();
			exportNpd.setFactory(factoryName);
			//get product allocation
			List<ProductAllocation> listPA = productAllocationService.findByCode(code);
			for (ProductAllocation pa : listPA) {
				String shortname = pa.getShortname();
				Distributor distributor = distributorService.findByShortname(shortname);
				if(distributor != null) {
					String area = distributor.getArea(); 
					String region = checkRegion(area);
					switch (region) {
					case Constants.NORTH:
						northAllocation += 1;
						break;
					case Constants.MIDDLE:
						centralAllocation += 1;
						break;
					case Constants.SOUTH:
						southAllocation += 1;
						break;
					default:
						break;
					}
				}
			}
			// get product sample
			List<Sample> listSample = sampleService.findByItem(code);
			Set<String> fullnameSample = new HashSet<String>();
			for (Sample sample : listSample) {
				String fullname = sample.getCustomer();
				if(!fullnameSample.contains(fullname)) {
					Distributor distributor = distributorService.findByFullname(fullname);
					if(distributor != null) {
						String area = distributor.getArea();
						String region = checkRegion(area);
						switch (region) {
						case Constants.NORTH:
							northSample += 1;
							break;
						case Constants.MIDDLE:
							centralSample += 1;
							break;
						case Constants.SOUTH:
							southSample += 1;
							break;
						default:
							break;
						}
						fullnameSample.add(fullname);
					}
				}
			}
			// get product sale
			List<SaleVolume> listSaleVolume = saleVolumeService.findByOrderitem(code);
			Set<String> fullnameSaleVolume = new HashSet<String>();
			for (SaleVolume saleVolume : listSaleVolume) {
				String fullname = saleVolume.getFullname();
				double sqmt = saleVolume.getSquaremeter();
				if(!fullnameSaleVolume.contains(fullname) && sqmt >= 10) {
					Distributor distributor = distributorService.findByFullname(fullname);
					if(distributor != null) {
						String area = distributor.getArea();
						String region = checkRegion(area);
						switch (region) {
						case Constants.NORTH:
							northSaleVolume += 1;
							break;
						case Constants.MIDDLE:
							centralSaleVolume += 1;
							break;
						case Constants.SOUTH:
							southSaleVolume += 1;
							break;
						default:
							break;
						}
						fullnameSaleVolume.add(fullname);
					}
				}
				if(sqmt >= 10) {
					ytd += sqmt;
				}
			}
			exportNpd.setNorthAllocation(northAllocation);
			exportNpd.setCentralAllocation(centralAllocation);
			exportNpd.setSouthAllocation(southAllocation);
			exportNpd.setNorthSample(northSample);
			exportNpd.setCentralSample(centralSample);
			exportNpd.setSouthSample(southSample);
			exportNpd.setNorthActual(northSaleVolume);
			exportNpd.setCentralActual(centralSaleVolume);
			exportNpd.setSouthActual(southSaleVolume);
			exportNpd.setYtd(ytd);
			exportNpd.setTotalAllocation(listPA.size());
			int northActualAllocation = 0;
			if(northAllocation > 0) {
				northActualAllocation = northSaleVolume * 100 / northAllocation;
			}
			exportNpd.setNorthActualAllocation(northActualAllocation);
			int centralActualAllocation = 0;
			if(centralAllocation > 0) {
				centralActualAllocation = centralSaleVolume * 100 / centralAllocation;
			}
			exportNpd.setCentralActualAllocation(centralActualAllocation);
			int southActionAllocation = 0;
			if(southAllocation > 0) {
				southActionAllocation = southSaleVolume * 100 / southAllocation;
			}
			exportNpd.setSouthActualAllocation(southActionAllocation);
			exportNPDs.add(exportNpd);
		}
		ByteArrayInputStream in = npdToExcel(exportNPDs);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Report.xlsx");
		return ResponseEntity.ok().headers(headers)
				.body(new InputStreamResource(in));
	}
	private String checkRegion(String area) {
		String region = Constants.NORTH;
		if(Constants.AREA_07.equalsIgnoreCase(area)||Constants.AREA_08.equalsIgnoreCase(area)||Constants.AREA_09.equalsIgnoreCase(area)||Constants.AREA_10.equalsIgnoreCase(area)) {
			region = Constants.MIDDLE;
		}else if (Constants.AREA_11.equalsIgnoreCase(area)) {
			region = Constants.SOUTH;
		}
		return region;
	}
	
	private ByteArrayInputStream nppToExcel(List<ExportNPP> listExportNPP) throws IOException{
		String[] COLUMNs = {"Nhà phân phối", "Phân bổ", "Triển khai","Phần Trăm", "MTD", "YTD"};
		try {
			Workbook workbook = new XSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Report");
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			// Row for Header
			Row headerRow = sheet.createRow(0);
			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}
			int rowIdx = 1;
			for (ExportNPP exportNPP : listExportNPP) {
				Row row = sheet.createRow(rowIdx++);
				row.createCell(0).setCellValue(exportNPP.getName());
				row.createCell(1).setCellValue(exportNPP.getAllocation());
				row.createCell(2).setCellValue(exportNPP.getSale());
				row.createCell(3).setCellValue(exportNPP.getPercent());
				row.createCell(4).setCellValue(exportNPP.getMtd());
				row.createCell(5).setCellValue(exportNPP.getYtd());
			}
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private ByteArrayInputStream areaToExcel(List<AreaExport> listAreaExport) throws IOException{
		String[] COLUMNs = {"Vùng", "Nhà máy", "Tháng NPD", "Năm NPD", "Kích thước", "Mã sản phẩm", "Tổng phân bổ", "Tổng triển khai", "phần trăm","ytd"};
		try {
			Workbook workbook = new XSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Report");
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			// Row for Header
			Row headerRow = sheet.createRow(0);
			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}
			int rowIdx = 1;
			for (AreaExport areaExport : listAreaExport) {
				Row row = sheet.createRow(rowIdx++);
				row.createCell(0).setCellValue(areaExport.getArea());
				row.createCell(1).setCellValue(areaExport.getFactory());
				row.createCell(2).setCellValue(areaExport.getNpdmonth());
				row.createCell(3).setCellValue(areaExport.getNpdmonth());
				row.createCell(4).setCellValue(areaExport.getSize());
				row.createCell(5).setCellValue(areaExport.getCode());
				row.createCell(6).setCellValue(areaExport.getTotalAllo());
				row.createCell(7).setCellValue(areaExport.getTotalSell());
				row.createCell(8).setCellValue(areaExport.getTotalPercent());
				row.createCell(9).setCellValue(areaExport.getSaleYear());
			}
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private ByteArrayInputStream regionToExcel(List<ExportRegion> listExportRegion) throws IOException {
		String[] COLUMNs = { "Miền", "Nhà máy", "Tháng NPD", "Năm NPD", "Kích thước", "Mã sản phẩm", "Tổng pb",
				"Tổng triển khai", "TK/PB", "Tổng Sản lượng (m2)", "MTD phân bổ", "MTD triển khai", "TK/PB",
				"MTD sản lượng(m2)", "YTD phân bổ", "YTD triển khai", "TK/PB", "YTD sản lượng (m2)" };
		try {
			Workbook workbook = new XSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Sum by region");
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			// Row for Header
			Row headerRow = sheet.createRow(0);
			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}
			int rowIdx = 1;
			for (ExportRegion exportRegion : listExportRegion) {
				Row row = sheet.createRow(rowIdx++);
				row.createCell(0).setCellValue(exportRegion.getRegion());
				row.createCell(1).setCellValue(exportRegion.getFactory());
				row.createCell(2).setCellValue(exportRegion.getNpdMonth());
				row.createCell(3).setCellValue(exportRegion.getNpdYear());
				row.createCell(4).setCellValue(exportRegion.getSize());
				row.createCell(5).setCellValue(exportRegion.getCode());
				row.createCell(6).setCellValue(exportRegion.getTotalNumAllo());
				row.createCell(7).setCellValue(exportRegion.getTotalNumSale());
				row.createCell(8).setCellValue(exportRegion.getTotalPercentSale()+ "%");
				row.createCell(9).setCellValue(exportRegion.getTotalSale());
				row.createCell(10).setCellValue(exportRegion.getMtdNumAllo());
				row.createCell(11).setCellValue(exportRegion.getMtdNumSale());
				row.createCell(12).setCellValue(exportRegion.getMtdPercentSale()+ "%");
				row.createCell(13).setCellValue(exportRegion.getMtdSale());
				row.createCell(14).setCellValue(exportRegion.getYtdNumAllo());
				row.createCell(15).setCellValue(exportRegion.getYtdNumSale());
				row.createCell(16).setCellValue(exportRegion.getYtdPercentSale()+ "%");
				row.createCell(17).setCellValue(exportRegion.getYtdSale());
			}
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private ByteArrayInputStream npdToExcel(List<ExportNPD> listExportNPD)
			throws IOException {
		String[] COLUMNs = { "Công ty", "Kích thước", "Mẫu được chọn", "Tháng NPD","Tổng phân bổ/ Total Allocation",
				"Phân bổ/Allocation", "Cấp mẫu/Sample","Triển khai/Actual","TK/PB(Actual/Allocation)","Phân bổ/Allocation", "Cấp mẫu/Sample","Triển khai/Actual","TK/PB(Actual/Allocation)","Phân bổ/Allocation", "Cấp mẫu/Sample","Triển khai/Actual","TK/PB(Actual/Allocation)","YTD Sale Volume(m2)"};
		try {
			Workbook workbook = new XSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Report");
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			// Row for Header
			Row headerRow = sheet.createRow(0);
			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}
			int rowIdx = 1;
			for (ExportNPD exportNpd : listExportNPD) {
				Row row = sheet.createRow(rowIdx++);
				row.createCell(0).setCellValue(exportNpd.getFactory());
				row.createCell(1).setCellValue(exportNpd.getSize());
				row.createCell(2).setCellValue(exportNpd.getCode());
				row.createCell(3).setCellValue(exportNpd.getMonthNPD());
				row.createCell(4).setCellValue(exportNpd.getTotalAllocation());
				row.createCell(5).setCellValue(exportNpd.getNorthAllocation());
				row.createCell(6).setCellValue(exportNpd.getNorthSample());
				row.createCell(7).setCellValue(exportNpd.getNorthActual());
				row.createCell(8).setCellValue(exportNpd.getNorthActualAllocation() + "%");
				row.createCell(9).setCellValue(exportNpd.getCentralAllocation());
				row.createCell(10).setCellValue(exportNpd.getCentralSample());
				row.createCell(11).setCellValue(exportNpd.getCentralActual());
				row.createCell(12).setCellValue(exportNpd.getCentralActualAllocation() + "%");
				row.createCell(13).setCellValue(exportNpd.getSouthAllocation());
				row.createCell(14).setCellValue(exportNpd.getSouthSample());
				row.createCell(15).setCellValue(exportNpd.getSouthActual());
				row.createCell(16).setCellValue(exportNpd.getSouthActualAllocation() + "%");
				row.createCell(17).setCellValue(exportNpd.getYtd());
			}
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private List<String> getYears(){
		List<String> years = new ArrayList<String>();
		years.add("2021");
		years.add("2020");
		years.add("2019");
		return years;
	}
	
	private List<String> getMonths(){
		List<String> months = new ArrayList<String>();
		months.add("--");
		months.add("01");
		months.add("02");
		months.add("03");
		months.add("04");
		months.add("05");
		months.add("06");
		months.add("07");
		months.add("08");
		months.add("09");
		months.add("10");
		months.add("11");
		months.add("12");
		return months;
	}
	
	private boolean containsName(final List<SaleVolume> list, final String fullname){
	    return list.stream().filter(o -> o.getFullname().equals(fullname)).findFirst().isPresent();
	}
}
