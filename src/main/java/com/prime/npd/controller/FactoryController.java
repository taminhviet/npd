package com.prime.npd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.prime.npd.model.Factory;
import com.prime.npd.service.FactoryService;

@Controller
public class FactoryController {
	
	@Autowired
	FactoryService factoryService;

	@RequestMapping(value = "/admin/factory", method = RequestMethod.GET)
	public String factory(Model model) {
		List<Factory> lsFactory = factoryService.getAllFact();
		if(lsFactory != null && !lsFactory.isEmpty()){
			model.addAttribute("lsFactory", lsFactory);
		}
		return "factory";
	}
	
	@RequestMapping(value = "/admin/addfactory", method = RequestMethod.POST)
	public String addfactory(Model model,
			@RequestParam("id") String id,
			@RequestParam("name") String name,
			@RequestParam("phonenumber") String phonenumber,
			@RequestParam("email") String email,
			@RequestParam("address") String address){
		Factory factory = new Factory(id, name, address, phonenumber, email);
		factoryService.saveFact(factory);
		return "redirect:/admin/factory";
	}
	
	
}
