package com.prime.npd.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.prime.npd.model.Area;
import com.prime.npd.model.AreaDistributor;
import com.prime.npd.model.Distributor;
import com.prime.npd.model.DistributorChanged;
import com.prime.npd.model.ExportRegion;
import com.prime.npd.model.Factory;
import com.prime.npd.model.FirstSale;
import com.prime.npd.model.Product;
import com.prime.npd.model.ProductAllocation;
import com.prime.npd.model.SaleVolume;
import com.prime.npd.model.Sample;
import com.prime.npd.model.SampleDisTable;
import com.prime.npd.model.User;
import com.prime.npd.service.AreaService;
import com.prime.npd.service.AuthenLogService;
import com.prime.npd.service.DistributorChangedService;
import com.prime.npd.service.DistributorService;
import com.prime.npd.service.FactoryService;
import com.prime.npd.service.FirstSaleService;
import com.prime.npd.service.ProductAllocationService;
import com.prime.npd.service.ProductService;
import com.prime.npd.service.SaleVolumeService;
import com.prime.npd.service.SampleService;
import com.prime.npd.service.UserService;
import com.prime.npd.util.Constants;

@Controller
public class SaleController {

	@Autowired
	UserService userService;
	
	@Autowired
	DistributorService distributorService;
	
	@Autowired
	SampleService sampleService;
	
	@Autowired
	SaleVolumeService saleVolumeService;
	
	@Autowired
	ProductAllocationService productAllocationService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	FactoryService factoryService;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	AuthenLogService authenLogService;
	
	@Autowired
	FirstSaleService firstSaleService;
	
	@Autowired
	DistributorChangedService distributorChangedService;
	
	
	@RequestMapping(value = "/saleprofile", method = RequestMethod.POST)
	public @ResponseBody User saleprofile(Model model,
			@RequestParam(name = "username", required = true) String username) {
		User user = userService.findbyUsername(username);
		return user;
	}
	@RequestMapping(value = "/saledistributor", method = RequestMethod.POST)
	public @ResponseBody List<Distributor> saledistributor(Model model,
			@RequestParam(name = "username", required = true) String username) {
		List<Distributor> distributors = distributorService.findBySale(username);
		return distributors;
	}

	@RequestMapping(value = "/sale", method = RequestMethod.GET)
	public String sale(Model model){
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		model.addAttribute("user", user);
		
		if(currentPrincipalName.contains("marketing")){
			List<AreaDistributor> areaDistributors = new ArrayList<AreaDistributor>();
			List<Area> areas = areaService.findAllArea();
			for (Area area : areas) {
				List<Distributor> distributors = distributorService.findByArea(area.getName());
				if(distributors != null && !distributors.isEmpty()){
					AreaDistributor areaDistributor = new AreaDistributor(area.getName(), distributors);
					areaDistributors.add(areaDistributor);
				}
			}
			Collections.sort(areaDistributors, new Comparator<AreaDistributor>() {

				@Override
				public int compare(AreaDistributor o1, AreaDistributor o2) {
					String area1 = o1.getArea();
					String area2 = o2.getArea();
					return area1.compareTo(area2);
				}
				
			});
			model.addAttribute("areaDistributors", areaDistributors);
			List<String> years = getYearSM();
			List<String> months = getMonths();
			model.addAttribute("years", years);
			model.addAttribute("months", months);
			return "salemarketing";
		}
		List<Distributor> distributors = distributorService.findBySale(currentPrincipalName);
		if(distributors != null && !distributors.isEmpty()){
			model.addAttribute("distributors", distributors);
		}
		return "sale";
	}
	
	
	@RequestMapping(value = "/sale/salechangepw", method = RequestMethod.GET)
	public String salechangepw(Model model){
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		model.addAttribute("user", user);
		model.addAttribute("error", Constants.EMPTY);
		model.addAttribute("success", Constants.EMPTY);
		return "salechangepw";
	}
	@RequestMapping(value = "/sale/changepw", method = RequestMethod.POST)
	public String changepw(Model model,
			@RequestParam(name = "oldpw") String oldpw,
			@RequestParam(name = "newpw") String newpw,
			@RequestParam(name = "renewpw") String renewpw){
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		model.addAttribute("user", user);
		String error = Constants.EMPTY;
		boolean isMatch = userService.checkMatch(user, oldpw);
		if(!isMatch){
			error = Constants.PW_WRONG;
			model.addAttribute("error", error);
			return "salechangepw";
		}
		if(!renewpw.equals(newpw)){
			error = Constants.PW_NOTMATCH;
			model.addAttribute("error", error);
			return "salechangepw";
		}
		if(newpw.length() < 6){
			error = Constants.PW_LENGTH;
			model.addAttribute("error", error);
			return "salechangepw";
		}
		user.setPassword(newpw);
		userService.saveUser(user);
		model.addAttribute("success", Constants.PW_SUCCESS);
		model.addAttribute("error", error);
		return "salechangepw";
	}
	
	@RequestMapping(value = "/sale/searchsample", method = RequestMethod.POST)
	public String searchSample(Model model,@RequestParam(name = "id", required= false) String id,
			@RequestParam(name = "month", required= false) String month,
			@RequestParam(name = "year", required= false) String year,
			@RequestParam(name = "factory", required= false) String factory,
			@RequestParam(name = "size", required= false) String size){
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		Distributor distributor = distributorService.findById(id);
		model.addAttribute("distributor", distributor);
		model.addAttribute("user", user);
		String customer = distributor.getFullname();
		String taxid = distributor.getId();
		DistributorChanged distributorChanged = distributorChangedService.findById(taxid);
		String shortname  = distributor.getShortname();
		NumberFormat formatter = new DecimalFormat("#0.00");
		Calendar ca  = Calendar.getInstance();
		int nyear = ca.get(Calendar.YEAR);
		int nmonth = ca.get(Calendar.MONTH)+1;
		if(year.contains(Constants.MINUS)){
			year = String.valueOf(nyear);
		}
		String paraMonth = month;
		if(month.contains(Constants.MINUS)){
			if(nmonth < 10){
				month = "0"+(nmonth);
			}else {
				month = String.valueOf(nmonth);
			}
		}
		String currentMonth = year + Constants.DOT + month;
		List<ProductAllocation> productAllocations = new ArrayList<ProductAllocation>();
		//only select year
		if (paraMonth.contains(Constants.MINUS)) {
			productAllocations = productAllocationService.searchByYearName(shortname, year);
			if(distributorChanged != null) {
				String oldShortname = distributorChanged.getOldshortname();
				if(!oldShortname.contains(Constants.COMMA)) {
					List<ProductAllocation> productAllocationsOld = productAllocationService.searchByYearName(oldShortname, year);
					productAllocations.addAll(productAllocationsOld);
				}
			}
		}else {
			productAllocations = productAllocationService.searchByMonthName(shortname, currentMonth);
			if(distributorChanged != null) {
				String oldShortname = distributorChanged.getOldshortname();
				if(!oldShortname.contains(Constants.COMMA)) {
					List<ProductAllocation> productAllocationsOld = productAllocations = productAllocationService.searchByMonthName(oldShortname, currentMonth);
					productAllocations.addAll(productAllocationsOld);
				}
			}
		}
		if(productAllocations != null && !productAllocations.isEmpty()){
			List<SampleDisTable> sampledisTables = new ArrayList<SampleDisTable>();
			Set<String> sizeSku = new HashSet<String>();
			Set<String> saleSku = new HashSet<String>();
			for (ProductAllocation pa : productAllocations) {
				SampleDisTable sampleDisTable = new SampleDisTable();
				String code = pa.getCode();
				Product product = productService.findByCode(code);
				if(product != null){
					sampleDisTable.setSkuimage(product.getUrl());
				}
				sampleDisTable.setSku(code);
				//check factory
				if(!factory.contains(Constants.MINUS)){
					if(!code.startsWith(factory)){
						continue;
					}
				}
				//check size
				if(!size.contains(Constants.MINUS)){
					if(!code.contains(size)){
						continue;
					}
				}
				List<Sample> samples = sampleService.searchByCI(customer, code);
				if(samples != null && !samples.isEmpty()){
					sampleDisTable.setDateSample(samples.get(0).getDate());
				}
				List<SaleVolume> saleVl = saleVolumeService.findByDis(code, customer);
				
				if(distributorChanged != null) {
					String oldFullname = distributorChanged.getOldfullname();
					if(!oldFullname.contains(Constants.COMMA)) {
						List<SaleVolume> saleV2 = saleVolumeService.findByDis(code, oldFullname);
						saleVl.addAll(saleV2);
					}
				}
				double ytd = 0;
				double mtd = 0;
				if(saleVl != null && !saleVl.isEmpty()){
					for (SaleVolume saleVolume : saleVl) {
						double sqmt = saleVolume.getSquaremeter();
						String saleMonth = saleVolume.getMonth();
						if(saleMonth.contains(String.valueOf(year))){
							ytd = ytd + sqmt;
						}
						if(currentMonth.equalsIgnoreCase(saleMonth)){
							mtd = mtd+sqmt;
						}
					}
				}
				
				FirstSale firstSale = firstSaleService.findByCode(code);
				if(firstSale != null) {
					sampleDisTable.setFirstsale(firstSale.getFirstDate());
				}
				String ytdString = formatter.format(ytd);
				String mtdString = formatter.format(mtd);
				sampleDisTable.setYtd(Double.valueOf(ytdString));
				sampleDisTable.setMtd(Double.valueOf(mtdString));
				sampleDisTable.setAllocated(true);
				sampledisTables.add(sampleDisTable);
				sizeSku.add(code);
				if (paraMonth.contains(Constants.MINUS)) {
					if(sampleDisTable.getYtd() > 10){
						saleSku.add(code);
					}
				}else {
					if(sampleDisTable.getMtd() > 10){
						saleSku.add(code);
					}
				}
			}
			if(!sampledisTables.isEmpty()){
				model.addAttribute("sampledisTables", sampledisTables);
				int skunumber = sizeSku.size(); 
				int salesku = saleSku.size();
				int percent = salesku * 100 / skunumber;
				model.addAttribute("skunumber", skunumber);
				model.addAttribute("salesku", salesku);
				model.addAttribute("percent", percent);
			}
		}
		List<String> years = getYears();
		List<String> months = getMonths();
		model.addAttribute("years", years);
		if(!year.contains(Constants.MINUS)){
			model.addAttribute("dYear", year);
		}
		model.addAttribute("months", months);
		if(!paraMonth.contains(Constants.MINUS)){
			model.addAttribute("dMonth", month);
		}
		List<Factory> lsFactory = new ArrayList<Factory>();
		Factory defaultfac = new Factory();
		defaultfac.setId("--");
		defaultfac.setName("------");
		lsFactory.add(defaultfac);
		lsFactory.addAll(factoryService.getAllFact());
		model.addAttribute("lsFactory", lsFactory);
		if(!factory.contains(Constants.MINUS)) {
			model.addAttribute("dFactory", factory);
		}
		
		List<Product> products = productService.getAllProducts();
		Set<String> pdSize = new HashSet<String>();
		for (Product product : products) {
			String sizepd = product.getSize();
			if(size != null) {
				pdSize.add(sizepd);
			}
		}
		List<String> productSize = new ArrayList<String>(pdSize);
		productSize.add("------");
		Collections.sort(productSize);
		model.addAttribute("productSize", productSize);
		if(!size.contains(Constants.MINUS)) {
			model.addAttribute("dSize", size);
		}
		return "saledistributor";
	}
	
	@RequestMapping(value = "/distributor/searchsample", method = RequestMethod.POST)
	public @ResponseBody List<SampleDisTable> searchSampleAPI(Model model,@RequestParam(name = "id", required= false) String id,
			@RequestParam(name = "month", required= false) String month,
			@RequestParam(name = "year", required= false) String year,
			@RequestParam(name = "factory", required= false) String factory,
			@RequestParam(name = "size", required= false) String size){
		Distributor distributor = distributorService.findById(id);
		String taxid = distributor.getId();
		String shortname  = distributor.getShortname();
		String customer = distributor.getFullname();
		DistributorChanged distributorChanged = distributorChangedService.findById(taxid);
		NumberFormat formatter = new DecimalFormat("#0.00");
		Calendar ca  = Calendar.getInstance();
		int nyear = ca.get(Calendar.YEAR);
		int nmonth = ca.get(Calendar.MONTH)+1;
		if(year.contains(Constants.MINUS)){
			year = String.valueOf(nyear);
		}
		String paraMonth = month;
		if(month.contains(Constants.MINUS)){
			if(nmonth < 10){
				month = "0"+(nmonth);
			}else {
				month = String.valueOf(nmonth);
			}
		}
		String currentMonth = year + Constants.DOT + month;
		List<ProductAllocation> productAllocations = new ArrayList<ProductAllocation>();
		//only select year
		if (paraMonth.contains(Constants.MINUS)) {
			productAllocations = productAllocationService.searchByYearName(shortname, year);
			if(distributorChanged != null) {
				String oldShortname = distributorChanged.getOldshortname();
				if(!oldShortname.contains(Constants.COMMA)) {
					List<ProductAllocation> productAllocationsOld = productAllocationService.searchByYearName(oldShortname, year);
					productAllocations.addAll(productAllocationsOld);
				}
			}
		}else {
			productAllocations = productAllocationService.searchByMonthName(shortname, currentMonth);
			if(distributorChanged != null) {
				String oldShortname = distributorChanged.getOldshortname();
				if(!oldShortname.contains(Constants.COMMA)) {
					List<ProductAllocation> productAllocationsOld = productAllocations = productAllocationService.searchByMonthName(oldShortname, currentMonth);
					productAllocations.addAll(productAllocationsOld);
				}
			}
		}
		if(productAllocations != null && !productAllocations.isEmpty()){
			List<SampleDisTable> sampledisTables = new ArrayList<SampleDisTable>();
			for (ProductAllocation pa : productAllocations) {
				SampleDisTable sampleDisTable = new SampleDisTable();
				String code = pa.getCode();
				Product product = productService.findByCode(code);
				if(product != null){
					sampleDisTable.setSkuimage(product.getUrl());
				}
				sampleDisTable.setSku(code);
				//check factory
				if(!factory.contains(Constants.MINUS)){
					if(!code.startsWith(factory)){
						continue;
					}
				}
				//check size
				if(!size.contains(Constants.MINUS)){
					if(!code.contains(size)){
						continue;
					}
				}
				List<Sample> samples = sampleService.searchByCI(customer, code);
				if(samples != null && !samples.isEmpty()){
					sampleDisTable.setDateSample(samples.get(0).getDate());
				}
				List<SaleVolume> saleVl = saleVolumeService.findByDis(code, customer);
				
				if(distributorChanged != null) {
					String oldFullname = distributorChanged.getOldfullname();
					if(!oldFullname.contains(Constants.COMMA)) {
						List<SaleVolume> saleV2 = saleVolumeService.findByDis(code, oldFullname);
						saleVl.addAll(saleV2);
					}
				}
				double ytd = 0;
				double mtd = 0;
				if(saleVl != null && !saleVl.isEmpty()){
					for (SaleVolume saleVolume : saleVl) {
						double sqmt = saleVolume.getSquaremeter();
						String saleMonth = saleVolume.getMonth();
						if(saleMonth.contains(String.valueOf(year))){
							ytd = ytd + sqmt;
						}
						if(currentMonth.equalsIgnoreCase(saleMonth)){
							mtd = mtd+sqmt;
						}
					}
				}
				
				FirstSale firstSale = firstSaleService.findByCode(code);
				if(firstSale != null) {
					sampleDisTable.setFirstsale(firstSale.getFirstDate());
				}
				String ytdString = formatter.format(ytd);
				String mtdString = formatter.format(mtd);
				sampleDisTable.setYtd(Double.valueOf(ytdString));
				sampleDisTable.setMtd(Double.valueOf(mtdString));
				sampleDisTable.setAllocated(true);
				sampledisTables.add(sampleDisTable);
//				sizeSku.add(code);
//				if (paraMonth.contains(Constants.MINUS)) {
//					if(sampleDisTable.getYtd() > 10){
//						saleSku.add(code);
//					}
//				}else {
//					if(sampleDisTable.getMtd() > 10){
//						saleSku.add(code);
//					}
//				}
			}
			if(!sampledisTables.isEmpty()){
				return sampledisTables;
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/distributor/{id}", method = RequestMethod.GET)
	public @ResponseBody List<SampleDisTable> apiDistributor(Model model, @PathVariable("id") String id){
		Distributor distributor = distributorService.findById(id);
		String taxid = distributor.getId();
		String shortname  = distributor.getShortname();
		String customer = distributor.getFullname();
		DistributorChanged distributorChanged = distributorChangedService.findById(taxid);
		List<ProductAllocation> productAllocations = productAllocationService.findByShortname(shortname);
		if(distributorChanged != null) {
			String oldShortname = distributorChanged.getOldshortname();
			if(!oldShortname.contains(Constants.COMMA)) {
				List<ProductAllocation> productAllocationsOld = productAllocationService.findByShortname(oldShortname);
				productAllocations.addAll(productAllocationsOld);
			}
		}
		NumberFormat formatter = new DecimalFormat("#0.00");    
		Calendar ca = Calendar.getInstance();
		int month = ca.get(Calendar.MONTH)+1;
		String monthStr = String.valueOf(month);
		if(month < 10){
			monthStr = "0".concat(monthStr);
		}
		int year = ca.get(Calendar.YEAR);
		String currentMonth = year + Constants.DOT + monthStr;
		if(productAllocations != null && !productAllocations.isEmpty()){
			List<SampleDisTable> sampledisTables = new ArrayList<SampleDisTable>();
			for (ProductAllocation pa : productAllocations) {
				SampleDisTable sampleDisTable = new SampleDisTable();
				String code = pa.getCode();
				Product product = productService.findByCode(code);
				if(product != null){
					sampleDisTable.setSkuimage(product.getUrl());
				}
				sampleDisTable.setSku(code);
				List<Sample> samples = sampleService.searchByCI(customer, code);
				if(samples != null && !samples.isEmpty()){
					sampleDisTable.setDateSample(samples.get(0).getDate());
				}
				List<SaleVolume> saleVl = saleVolumeService.findByDis(code, customer);
				
				if(distributorChanged != null) {
					String oldFullname = distributorChanged.getOldfullname();
					if(!oldFullname.contains(Constants.COMMA)) {
						List<SaleVolume> saleV2 = saleVolumeService.findByDis(code, oldFullname);
						saleVl.addAll(saleV2);
					}
				}
				double ytd = 0;
				double mtd = 0;
				if(saleVl != null && !saleVl.isEmpty()){
					for (SaleVolume saleVolume : saleVl) {
						double sqmt = saleVolume.getSquaremeter();
						String saleMonth = saleVolume.getMonth();
						if(saleMonth.contains(String.valueOf(year))){
							ytd = ytd + sqmt;
						}
						if(currentMonth.equalsIgnoreCase(saleMonth)){
							mtd = mtd+sqmt;
						}
					}
				}
				FirstSale firstSale = firstSaleService.findByCode(code);
				if(firstSale != null) {
					sampleDisTable.setFirstsale(firstSale.getFirstDate());
				}
				String ytdString = formatter.format(ytd);
				String mtdString = formatter.format(mtd);
				sampleDisTable.setYtd(Double.valueOf(ytdString));
				sampleDisTable.setMtd(Double.valueOf(mtdString));
				sampleDisTable.setAllocated(true);
				sampledisTables.add(sampleDisTable);
			}
			if(!sampledisTables.isEmpty()){
				return sampledisTables;
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/sale/distributor/{id}", method = RequestMethod.GET)
	public String saleDistributor(Model model, @PathVariable("id") String id){
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		Distributor distributor = distributorService.findById(id);
		model.addAttribute("distributor", distributor);
		model.addAttribute("user", user);
		String customer = distributor.getFullname();
		String taxid = distributor.getId();
		String shortname  = distributor.getShortname();
		DistributorChanged distributorChanged = distributorChangedService.findById(taxid);
		List<ProductAllocation> productAllocations = productAllocationService.findByShortname(shortname);
		if(distributorChanged != null) {
			String oldShortname = distributorChanged.getOldshortname();
			if(!oldShortname.contains(Constants.COMMA)) {
				List<ProductAllocation> productAllocationsOld = productAllocationService.findByShortname(oldShortname);
				productAllocations.addAll(productAllocationsOld);
			}
		}
		NumberFormat formatter = new DecimalFormat("#0.00");    
		Calendar ca = Calendar.getInstance();
		int month = ca.get(Calendar.MONTH)+1;
		String monthStr = String.valueOf(month);
		if(month < 10){
			monthStr = "0".concat(monthStr);
		}
		int year = ca.get(Calendar.YEAR);
		String currentMonth = year + Constants.DOT + monthStr;
		if(productAllocations != null && !productAllocations.isEmpty()){
			List<SampleDisTable> sampledisTables = new ArrayList<SampleDisTable>();
			Set<String> sizeSku = new HashSet<String>();
			Set<String> saleSku = new HashSet<String>();
			for (ProductAllocation pa : productAllocations) {
				SampleDisTable sampleDisTable = new SampleDisTable();
				String code = pa.getCode();
				Product product = productService.findByCode(code);
				if(product != null){
					sampleDisTable.setSkuimage(product.getUrl());
				}
				sampleDisTable.setSku(code);
				List<Sample> samples = sampleService.searchByCI(customer, code);
				if(samples != null && !samples.isEmpty()){
					sampleDisTable.setDateSample(samples.get(0).getDate());
				}
				List<SaleVolume> saleVl = saleVolumeService.findByDis(code, customer);
				
				if(distributorChanged != null) {
					String oldFullname = distributorChanged.getOldfullname();
					if(!oldFullname.contains(Constants.COMMA)) {
						List<SaleVolume> saleV2 = saleVolumeService.findByDis(code, oldFullname);
						saleVl.addAll(saleV2);
					}
				}
				double ytd = 0;
				double mtd = 0;
				if(saleVl != null && !saleVl.isEmpty()){
					for (SaleVolume saleVolume : saleVl) {
						double sqmt = saleVolume.getSquaremeter();
						String saleMonth = saleVolume.getMonth();
						if(saleMonth.contains(String.valueOf(year))){
							ytd = ytd + sqmt;
						}
						if(currentMonth.equalsIgnoreCase(saleMonth)){
							mtd = mtd+sqmt;
						}
					}
				}
				FirstSale firstSale = firstSaleService.findByCode(code);
				if(firstSale != null) {
					sampleDisTable.setFirstsale(firstSale.getFirstDate());
				}
				/**
				List<SaleVolume> saleVolumes = saleVolumeService.findByOrderitem(code);
				if(saleVolumes != null && !saleVolumes.isEmpty()){
					Collections.sort(saleVolumes, new Comparator<SaleVolume>() {

						@Override
						public int compare(SaleVolume o1, SaleVolume o2) {
							Date date1 = o1.getRequestdate();
							Date date2 = o2.getRequestdate();
							return date1.compareTo(date2);
						}
						
					});
					SaleVolume sv = saleVolumes.get(0);
					sampleDisTable.setFirstsale(sv.getRequestdate());
				}
				**/
				String ytdString = formatter.format(ytd);
				String mtdString = formatter.format(mtd);
				sampleDisTable.setYtd(Double.valueOf(ytdString));
				sampleDisTable.setMtd(Double.valueOf(mtdString));
				sampleDisTable.setAllocated(true);
				sampledisTables.add(sampleDisTable);
				sizeSku.add(code);
				if(sampleDisTable.getYtd() > 10){
					saleSku.add(code);
				}
			}
			if(!sampledisTables.isEmpty()){
				model.addAttribute("sampledisTables", sampledisTables);
				int skunumber = sizeSku.size(); 
				int salesku = saleSku.size();
				int percent = salesku * 100 / skunumber ;
				model.addAttribute("skunumber", skunumber);
				model.addAttribute("salesku", salesku);
				model.addAttribute("percent", percent);
			}
		}
		List<String> years = getYears();
		List<String> months = getMonths();
		model.addAttribute("years", years);
		model.addAttribute("months", months);
		List<Factory> lsFactory = new ArrayList<Factory>();
		Factory defaultfac = new Factory();
		defaultfac.setId("--");
		defaultfac.setName("------");
		lsFactory.add(defaultfac);
		lsFactory.addAll(factoryService.getAllFact());
		model.addAttribute("lsFactory", lsFactory);
		List<Product> products = productService.getAllProducts();
		Set<String> pdSize = new HashSet<String>();
		for (Product product : products) {
			String size = product.getSize();
			if(size != null) {
				pdSize.add(size);
			}
		}
		List<String> productSize = new ArrayList<String>(pdSize);
		productSize.add("------");
		Collections.sort(productSize);
		model.addAttribute("productSize", productSize);
		return "saledistributor";
	}
	@RequestMapping(value = "/sale/exportbyregion", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> exportRegion(Model model,
			@RequestParam(name = "month", required= false) String month,
			@RequestParam(name = "year", required= false) String year) throws IOException {
		List<Product> products = productService.getAllProducts();
		List<ExportRegion> lsExportRegion = new ArrayList<ExportRegion>();
		Calendar ca  = Calendar.getInstance();
		String nmonth = String.valueOf(ca.get(Calendar.MONTH)+1);
		if(!month.contains(Constants.MINUS)) {
			nmonth = month;
		}
		String yearNow = year;
		for (Product product : products) {
			String code = product.getCode();
			String npdMonth = product.getMonthnpd();
			String npdSize = product.getSize();
			String npdYear = npdMonth.substring(0, 4);
			// get factory
			String factCode = code.substring(0, 2);
			Factory factory = factoryService.findById(factCode);
			String factoryName = factory.getName();
			//init North
			ExportRegion northRegion = new ExportRegion();
			northRegion.setRegion(Constants.NORTH);
			northRegion.setFactory(factoryName);
			northRegion.setNpdMonth(npdMonth);
			northRegion.setNpdYear(npdYear);
			northRegion.setSize(npdSize);
			northRegion.setCode(code);
			int northAllocation = 0;
			int northSaleVolume = 0;
			double northTotalSale = 0;
			int northYtdAllocation = 0;
			int northYtdSaleVolumn = 0;
			double northYtdSale = 0;
			int northMtdAllocation = 0;
			int northMtdSaleVolumn = 0;
			double northMtdSale = 0;
			
			//init Central
			ExportRegion centralRegion = new ExportRegion();
			centralRegion.setRegion(Constants.MIDDLE);
			centralRegion.setFactory(factoryName);
			centralRegion.setNpdMonth(npdMonth);
			centralRegion.setNpdYear(npdYear);
			centralRegion.setSize(npdSize);
			centralRegion.setCode(code);
			int centralAllocation = 0;
			int centralSaleVolume = 0;
			double centralTotalSale = 0;
			int centralYtdAllocation = 0;
			int centralYtdSaleVolumn = 0;
			double centralYtdSale = 0;
			int centralMtdAllocation = 0;
			int centralMtdSaleVolumn = 0;
			double centralMtdSale = 0;
			
			//init South
			ExportRegion southRegion = new ExportRegion();
			southRegion.setRegion(Constants.SOUTH);
			southRegion.setFactory(factoryName);
			southRegion.setNpdMonth(npdMonth);
			southRegion.setNpdYear(npdYear);
			southRegion.setSize(npdSize);
			southRegion.setCode(code);
			int southAllocation = 0;
			int southSaleVolume = 0;
			double southTotalSale = 0;
			int southYtdAllocation = 0;
			int southYtdSaleVolumn = 0;
			double southYtdSale = 0;
			int southMtdAllocation = 0;
			int southMtdSaleVolumn = 0;
			double southMtdSale = 0;
			
			//get product allocation
			List<ProductAllocation> listPA = productAllocationService.findByCode(code);
			if(listPA != null && !listPA.isEmpty()) {
				for (ProductAllocation pa : listPA) {
					String shortname = pa.getShortname();
					String paMonth = pa.getMonth();
					Distributor distributor = distributorService.findByShortname(shortname);
					if(distributor != null) {
						String area = distributor.getArea(); 
						String region = checkRegion(area);
						switch (region) {
						case Constants.NORTH:
							northAllocation += 1;
							if(paMonth.contains(yearNow)) {
								northYtdAllocation +=1;
							}
							if(paMonth.contains(nmonth)) {
								northMtdAllocation +=1;
							}
							break;
						case Constants.MIDDLE:
							centralAllocation += 1;
							if(paMonth.contains(yearNow)) {
								centralYtdAllocation +=1;
							}
							if(paMonth.contains(nmonth)) {
								centralMtdAllocation +=1;
							}
							break;
						case Constants.SOUTH:
							southAllocation += 1;
							if(paMonth.contains(yearNow)) {
								southYtdAllocation +=1;
							}
							if(paMonth.contains(nmonth)) {
								southMtdAllocation +=1;
							}
							break;
						default:
							break;
						}
					}
//					else {
//						System.out.println(shortname);
//					}
				}
			}
			
			
			// get product sale
			List<SaleVolume> listSaleVolume = saleVolumeService.findByOrderitem(code);
			Set<String> fullnameSaleVolume = new HashSet<String>();
			for (SaleVolume saleVolume : listSaleVolume) {
				String fullname = saleVolume.getFullname();
				String monthSaleVolume = saleVolume.getMonth();
				double sqmt = saleVolume.getSquaremeter();
				Distributor distributor = distributorService.findByFullname(fullname);
				if(distributor != null && sqmt >= 10) {
					String area = distributor.getArea();
					String region = checkRegion(area);
					//add sale total
					switch (region) {
					case Constants.NORTH:
						northTotalSale += sqmt;
						if(monthSaleVolume.contains(yearNow)) {
							northYtdSale += sqmt;
						}
						if(monthSaleVolume.contains(nmonth)) {
							northMtdSale += sqmt;
						}
						break;
					case Constants.MIDDLE:
						centralTotalSale += sqmt;
						if(monthSaleVolume.contains(yearNow)) {
							centralYtdSale += sqmt;
						}
						if(monthSaleVolume.contains(nmonth)) {
							centralMtdSale += sqmt;
						}
						break;
					case Constants.SOUTH:
						southTotalSale += sqmt;
						if(monthSaleVolume.contains(yearNow)) {
							southYtdSale += sqmt;
						}
						if(monthSaleVolume.contains(nmonth)) {
							southMtdSale += sqmt;
						}
						break;
					default:
						break;
					}
					//count number of sale
					if(!fullnameSaleVolume.contains(fullname)) {
						switch (region) {
						case Constants.NORTH:
							northSaleVolume += 1;
							if(monthSaleVolume.contains(yearNow)) {
								northYtdSaleVolumn += 1;
							}
							if(monthSaleVolume.contains(nmonth)) {
								northMtdSaleVolumn += 1;
							}
							break;
						case Constants.MIDDLE:
							centralSaleVolume += 1;
							if(monthSaleVolume.contains(yearNow)) {
								centralYtdSaleVolumn += 1;
							}
							if(monthSaleVolume.contains(nmonth)) {
								centralMtdSaleVolumn += 1;
							}
							break;
						case Constants.SOUTH:
							southSaleVolume += 1;
							if(monthSaleVolume.contains(yearNow)) {
								southYtdSaleVolumn += 1;
							}
							if(monthSaleVolume.contains(nmonth)) {
								southMtdSaleVolumn += 1;
							}
							break;
						default:
							break;
						}
						fullnameSaleVolume.add(fullname);
					}
				}
			}
			//set total allo
			northRegion.setTotalNumAllo(northAllocation);
			centralRegion.setTotalNumAllo(centralAllocation);
			southRegion.setTotalNumAllo(southAllocation);
			
			//set total sale
			northRegion.setTotalNumSale(northSaleVolume);
			centralRegion.setTotalNumSale(centralSaleVolume);
			southRegion.setTotalNumSale(southSaleVolume);
			
			//set percent
			int northtotalPercentSale = 0;
			if(northAllocation > 0) {
				northtotalPercentSale = northSaleVolume * 100 / northAllocation;
			}
			northRegion.setTotalPercentSale(northtotalPercentSale);
			
			int centraltotalPercentSale = 0;
			if(centralAllocation > 0) {
				centraltotalPercentSale = centralSaleVolume * 100 / centralAllocation;
			}
			centralRegion.setTotalPercentSale(centraltotalPercentSale);
			
			int southtotalPercentSale = 0;
			if(southAllocation > 0) {
				southtotalPercentSale = southSaleVolume * 100 / southAllocation;
			}
			southRegion.setTotalPercentSale(southtotalPercentSale);
			
			//set total sale
			northRegion.setTotalSale(northTotalSale);
			centralRegion.setTotalSale(centralTotalSale);
			southRegion.setTotalSale(southTotalSale);
			
			//set ytd allo
			northRegion.setYtdNumAllo(northYtdAllocation);
			centralRegion.setYtdNumAllo(centralYtdAllocation);
			southRegion.setYtdNumAllo(southYtdAllocation);
			
			//set ytd num sale
			northRegion.setYtdNumSale(northYtdSaleVolumn);
			centralRegion.setYtdNumSale(centralYtdSaleVolumn);
			southRegion.setYtdNumSale(southYtdSaleVolumn);
			
			//set percent ytd
			int northYtdPercentSale = 0;
			if(northYtdAllocation > 0) {
				northYtdPercentSale = northYtdSaleVolumn * 100 / northYtdAllocation;
			}
			northRegion.setYtdPercentSale(northYtdPercentSale);
			
			int centralYtdPercentSale = 0;
			if(centralYtdPercentSale > 0) {
				centralYtdPercentSale = centralYtdSaleVolumn * 100 / centralYtdAllocation;
			}
			centralRegion.setYtdPercentSale(centralYtdPercentSale);
			
			int southYtdPercentSale = 0;
			if(southYtdAllocation > 0) {
				southYtdPercentSale = southYtdSaleVolumn * 100 / southYtdAllocation;
			}
			southRegion.setYtdPercentSale(southYtdPercentSale);
			
			//set total sale
			northRegion.setYtdSale(northYtdSale);
			centralRegion.setYtdSale(centralYtdSale);
			southRegion.setYtdSale(southYtdSale);
			
			//set mtd allo
			northRegion.setMtdNumAllo(northMtdAllocation);
			centralRegion.setMtdNumAllo(centralMtdAllocation);
			southRegion.setMtdNumAllo(southMtdAllocation);
			
			//set mtd num sale
			northRegion.setMtdNumSale(northMtdSaleVolumn);
			centralRegion.setMtdNumSale(centralMtdSaleVolumn);
			southRegion.setMtdNumSale(southMtdSaleVolumn);
			
			//set percent mtd
			int northMtdPercentSale = 0;
			if(northMtdAllocation > 0) {
				northMtdPercentSale = northMtdSaleVolumn * 100 / northMtdAllocation;
			}
			northRegion.setMtdPercentSale(northMtdPercentSale);
			
			int centralMtdPercentSale = 0;
			if(centralMtdPercentSale > 0) {
				centralMtdPercentSale = centralMtdSaleVolumn * 100 / centralMtdAllocation;
			}
			centralRegion.setMtdPercentSale(centralMtdPercentSale);
			
			int southMtdPercentSale = 0;
			if(southMtdAllocation > 0) {
				southMtdPercentSale = southMtdSaleVolumn * 100 / southMtdAllocation;
			}
			southRegion.setMtdPercentSale(southMtdPercentSale);
			
			//set total sale
			northRegion.setMtdSale(northMtdSale);
			centralRegion.setMtdSale(centralMtdSale);
			southRegion.setMtdSale(southMtdSale);
			
			lsExportRegion.add(northRegion);
			lsExportRegion.add(centralRegion);
			lsExportRegion.add(southRegion);
		}
		ByteArrayInputStream in = regionToExcel(lsExportRegion);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=ReportRegion.xlsx");
		return ResponseEntity.ok().headers(headers)
				.body(new InputStreamResource(in));
	}
	
	private ByteArrayInputStream regionToExcel(List<ExportRegion> listExportRegion) throws IOException {
		String[] COLUMNs = { "Miền", "Nhà máy", "Tháng NPD", "Năm NPD", "Kích thước", "Mã sản phẩm", "Tổng pb",
				"Tổng triển khai", "TK/PB", "Tổng Sản lượng (m2)", "MTD phân bổ", "MTD triển khai", "TK/PB",
				"MTD sản lượng(m2)", "YTD phân bổ", "YTD triển khai", "TK/PB", "YTD sản lượng (m2)" };
		try {
			Workbook workbook = new XSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Sum by region");
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			// Row for Header
			Row headerRow = sheet.createRow(0);
			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}
			int rowIdx = 1;
			for (ExportRegion exportRegion : listExportRegion) {
				Row row = sheet.createRow(rowIdx++);
				row.createCell(0).setCellValue(exportRegion.getRegion());
				row.createCell(1).setCellValue(exportRegion.getFactory());
				row.createCell(2).setCellValue(exportRegion.getNpdMonth());
				row.createCell(3).setCellValue(exportRegion.getNpdYear());
				row.createCell(4).setCellValue(exportRegion.getSize());
				row.createCell(5).setCellValue(exportRegion.getCode());
				row.createCell(6).setCellValue(exportRegion.getTotalNumAllo());
				row.createCell(7).setCellValue(exportRegion.getTotalNumSale());
				row.createCell(8).setCellValue(exportRegion.getTotalPercentSale()+ "%");
				row.createCell(9).setCellValue(exportRegion.getTotalSale());
				row.createCell(10).setCellValue(exportRegion.getMtdNumAllo());
				row.createCell(11).setCellValue(exportRegion.getMtdNumSale());
				row.createCell(12).setCellValue(exportRegion.getMtdPercentSale()+ "%");
				row.createCell(13).setCellValue(exportRegion.getMtdSale());
				row.createCell(14).setCellValue(exportRegion.getYtdNumAllo());
				row.createCell(15).setCellValue(exportRegion.getYtdNumSale());
				row.createCell(16).setCellValue(exportRegion.getYtdPercentSale()+ "%");
				row.createCell(17).setCellValue(exportRegion.getYtdSale());
			}
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	private String checkRegion(String area) {
		String region = Constants.NORTH;
		if(Constants.AREA_07.equalsIgnoreCase(area)||Constants.AREA_08.equalsIgnoreCase(area)||Constants.AREA_09.equalsIgnoreCase(area)||Constants.AREA_10.equalsIgnoreCase(area)) {
			region = Constants.MIDDLE;
		}else if (Constants.AREA_11.equalsIgnoreCase(area)) {
			region = Constants.SOUTH;
		}
		return region;
	}
	
	private List<String> getYears(){
		List<String> years = new ArrayList<String>();
		years.add("----");
		years.add("2021");
		years.add("2020");
		years.add("2019");
		return years;
	}
	private List<String> getYearSM(){
		List<String> years = new ArrayList<String>();
		years.add("2021");
		years.add("2020");
		years.add("2019");
		return years;
	}
	
	private List<String> getMonths(){
		List<String> months = new ArrayList<String>();
		months.add("--");
		months.add("01");
		months.add("02");
		months.add("03");
		months.add("04");
		months.add("05");
		months.add("06");
		months.add("07");
		months.add("08");
		months.add("09");
		months.add("10");
		months.add("11");
		months.add("12");
		return months;
	}
	private boolean containsSKU(final List<SampleDisTable> list, final String code){
	    return list.stream().filter(o -> o.getSku().equals(code)).findFirst().isPresent();
	}
}
