package com.prime.npd.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.prime.npd.model.Product;
import com.prime.npd.model.ProductSize;
import com.prime.npd.service.ProductService;
import com.prime.npd.service.ProductSizeService;
import com.prime.npd.util.Constants;

@Controller
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	ProductSizeService productSizeService;

	@RequestMapping(value = "/admin/product", method = RequestMethod.GET)
	public String product(Model model) {
		List<Product> products = productService.getAllProducts();
		if(products != null && !products.isEmpty()){
			model.addAttribute("products", products);
		}
		return "product";
	}
	@RequestMapping(value = "/admin/deleteproduct", method = RequestMethod.GET)
	public String deleteproduct(Model model) {
		productService.deleteAllproduct();
		return "product";
	}
	@RequestMapping(value = "/admin/correctproduct", method = RequestMethod.GET)
	public String correctProduct(Model model) {
		List<Product> lsSizeNull = productService.findBySize(null);
		if(lsSizeNull != null && !lsSizeNull.isEmpty()) {
			for (Product product : lsSizeNull) {
				String code = product.getCode();
				String size = code.substring(3,9);
				product.setSize(size);
				productService.saveProduct(product);
			}
		}
		List<Product> lsFactoryNull = productService.findByFactory(null);
		if(lsFactoryNull != null && !lsFactoryNull.isEmpty()) {
			for (Product product : lsFactoryNull) {
				String code = product.getCode();
				String factory = code.substring(0,2);
				product.setFactory(factory);
				productService.saveProduct(product);
			}
		}
		return "redirect:/admin/product";
	}
	
	@RequestMapping(value = "/admin/uploadproduct", method = RequestMethod.POST)
	public String uploadproduct(@RequestParam("file") MultipartFile file,
			HttpServletRequest request) {
		List<Product> products = new ArrayList<Product>();
		Set<String> sizes = new HashSet<String>();
		try {
			String nameFile = file.getOriginalFilename();
			InputStream excelFile = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				int rowindex = currentRow.getRowNum();
				if(rowindex == 0){
					continue;
				}
				Product product = new Product();
				Iterator<Cell> cellIterator = currentRow.iterator();
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int colindex = currentCell.getColumnIndex();
					switch (colindex) {
					case 1:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							product.setFactory(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf((int)currentCell.getNumericCellValue());
							product.setFactory(cellValue);
						}
						break;
						
					case 3:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							product.setSize(cellValue);
							sizes.add(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf((int)currentCell.getNumericCellValue());
							product.setSize(cellValue);
							sizes.add(cellValue);
						}
						break;
						
					case 4:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							product.setCode(cellValue);
							if(cellValue.contains(Constants.DOT)){
								String endURL = Constants.URL_PRIME.concat(cellValue.replace(Constants.DOT, Constants.MINUS));
								product.setUrl(endURL);
							}
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf((int)currentCell.getNumericCellValue());
							product.setCode(cellValue);
							if(cellValue.contains(Constants.DOT)){
								String endURL = Constants.URL_PRIME.concat(cellValue.replace(Constants.DOT, Constants.MINUS));
								product.setUrl(endURL);
							}
						}
						break;
					case 5:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							product.setMonthnpd(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf((int)currentCell.getNumericCellValue());
							product.setMonthnpd(cellValue);
						}
						break;
					case 6:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							product.setClassify(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf((int)currentCell.getNumericCellValue());
							product.setClassify(cellValue);
						}
						break;
					case 7:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							product.setDescription(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf((int)currentCell.getNumericCellValue());
							product.setDescription(cellValue);
						}
						break;
					case 8:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							product.setEffect(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf((int)currentCell.getNumericCellValue());
							product.setEffect(cellValue);
						}
						break;
					default:
						break;
					}	
					
				}
				products.add(product);
			}
			if(!products.isEmpty()){
				for (Product product : products) {
					Product existProduct = productService.findByCode(product.getCode());
					if(existProduct == null) {
						productService.saveProduct(product);
					}
				}
			}
			if(!sizes.isEmpty()){
				for (String size : sizes) {
					ProductSize productSizeExist = productSizeService.findBySize(size);
					if(productSizeExist == null){
						ProductSize newSize = new ProductSize();
						newSize.setSize(size);
						productSizeService.saveProductSize(newSize);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/product";
	}
}
