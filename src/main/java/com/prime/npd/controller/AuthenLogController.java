package com.prime.npd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.prime.npd.model.AuthenLog;
import com.prime.npd.service.AuthenLogService;

@Controller
public class AuthenLogController {
	
	@Autowired
	AuthenLogService authenLogService;

	@RequestMapping(value = "/admin/authenlog", method = RequestMethod.GET)
	public String log(Model model){
		List<AuthenLog> authenLogs = authenLogService.getAll();
		if(authenLogs != null && !authenLogs.isEmpty()) {
			model.addAttribute("authenLogs", authenLogs);
		}
		return "authenlog";
	}
	@RequestMapping(value = "/admin/searchlog", method = RequestMethod.POST)
	public String searchlog(Model model,
			@RequestParam("username") String username){
		List<AuthenLog> authenLogs = authenLogService.findbyuser(username);
		if(authenLogs != null && !authenLogs.isEmpty()) {
			model.addAttribute("authenLogs", authenLogs);
		}
		return "authenlog";
	}
}
