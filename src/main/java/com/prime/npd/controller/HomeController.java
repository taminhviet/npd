package com.prime.npd.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.prime.npd.model.AuthenLog;
import com.prime.npd.model.User;
import com.prime.npd.service.AuthenLogService;
import com.prime.npd.service.UserService;
import com.prime.npd.util.Constants;

@Controller
public class HomeController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	AuthenLogService authenLogService;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Model model) {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentPrincipalName = authentication.getName();
		if (currentPrincipalName.equalsIgnoreCase("anonymousUser")) {
			return "accessdenied";
		}
		User user = userService.findbyUsername(currentPrincipalName);
		if (Constants.ROLE_ADMIN == user.getIsAdmin()) {
			return "redirect:/admin";
		} else {
			AuthenLog authenLog = new AuthenLog();
			Date newDate = new Date();
			authenLog.setDate(newDate);
			authenLog.setUsername(currentPrincipalName);
			authenLogService.saveLog(authenLog);
			return "redirect:/sale";
		}
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(Model model) {
		return "login";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Model model) {
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(Model model,
			@RequestParam("username") String username,
			@RequestParam("password") String password,
			@RequestParam(name = "phone", required = false) String phone,
			@RequestParam(name = "fullname", required = false) String fullname,
			@RequestParam(name = "address", required = false) String address) {
		User user = new User();
		if (username.contains("admin")) {
			user.setIsAdmin(Constants.ROLE_ADMIN);
		} else {
			user.setIsAdmin(Constants.ROLE_SALE);
		}
		user.setUsername(username);
		user.setPassword(password);
		user.setPhone(phone);
		user.setFullname(fullname);
		user.setAddress(address);
		userService.saveUser(user);
		return "login";
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String error(Model model) {
		return "error";
	}

	@RequestMapping(value = "/access-denied", method = RequestMethod.GET)
	public String access(Model model) {
		return "accessdenied";
	}
}
