package com.prime.npd.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.prime.npd.model.Area;
import com.prime.npd.model.Distributor;
import com.prime.npd.model.DistributorChanged;
import com.prime.npd.model.SaleData;
import com.prime.npd.model.User;
import com.prime.npd.service.AreaService;
import com.prime.npd.service.DistributorChangedService;
import com.prime.npd.service.DistributorService;
import com.prime.npd.service.UserService;
import com.prime.npd.util.Constants;

@Controller
public class DistributorController {

	@Autowired
	UserService userService;
	
	@Autowired
	DistributorService distributorService;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	DistributorChangedService distributorChangedService;
	
	@RequestMapping(value = "/admin/adddistributor", method = RequestMethod.POST)
	public String adddistributor(Model model,@RequestParam("id") String id,
			@RequestParam("fullname") String fullname,
			@RequestParam("shortname") String shortname,
			@RequestParam("area") String area,
			@RequestParam("province") String province,
			@RequestParam("address") String address,
			@RequestParam(name = "email", required = false) String email,
			@RequestParam(name = "phone", required = false) String phone,
			@RequestParam("sale") String sale){
		Distributor distributor = new Distributor();
		distributor.setId(id.trim());
		distributor.setFullname(fullname);
		distributor.setShortname(shortname);
		distributor.setArea(area);
		distributor.setProvince(province);
		distributor.setAddress(address);
		distributor.setEmail(email);
		distributor.setPhone(phone);
		distributor.setSale(sale);
		distributorService.saveDistributor(distributor);
		return "redirect:/admin";
	}
	@RequestMapping(value = "/admin/editdistributor", method = RequestMethod.POST)
	public String editdistributor(Model model,@RequestParam("editid") String ideditid,
			@RequestParam("editfullname") String editfullname,
			@RequestParam("editshortname") String editshortname,
			@RequestParam("editaddress") String editaddress,
			@RequestParam(name = "editemail", required = false) String editemail,
			@RequestParam(name = "editphone", required = false) String editphone,
			@RequestParam("editsale") String editsale){
		Distributor existDis = distributorService.findById(ideditid);
		if(existDis != null){
			String existFullname = existDis.getFullname();
			String existShortname = existDis.getShortname();
			// change name
			if(!existFullname.equalsIgnoreCase(editfullname.trim()) || !existShortname.equalsIgnoreCase(editshortname.trim())) {
				DistributorChanged checkExist = distributorChangedService.findById(ideditid);
				//create 1st record changed
				if(checkExist == null) {
					DistributorChanged distributorChanged = new DistributorChanged();
					distributorChanged.setId(ideditid);
					distributorChanged.setCurrentfullname(editfullname.trim());
					distributorChanged.setCurrentshortname(editshortname.trim());
					distributorChanged.setOldfullname(existFullname);
					distributorChanged.setOldshortname(existShortname);
					distributorChangedService.saveDistributorChanged(distributorChanged);
				}else {
					// update new changed record
					checkExist.setCurrentfullname(editfullname.trim());
					checkExist.setCurrentshortname(editshortname.trim());
					if(!existFullname.contains(editfullname)) {
						String oldFullName = existFullname.concat(Constants.COMMA).concat(editfullname.trim());
						checkExist.setOldfullname(oldFullName);
					}
					if(existShortname.contains(editshortname)) {
						String oldShortName = existShortname.concat(Constants.COMMA).concat(editshortname.trim());
						checkExist.setOldshortname(oldShortName);
					}
					distributorChangedService.saveDistributorChanged(checkExist);
				}
			}
			existDis.setFullname(editfullname.trim());
			existDis.setShortname(editshortname.trim());
			existDis.setAddress(editaddress.trim());
			existDis.setEmail(editemail.trim());
			existDis.setPhone(editphone.trim());
			existDis.setSale(editsale.trim());
			distributorService.saveDistributor(existDis);
		}
		return "redirect:/admin";
	}
	
	@RequestMapping(value = "/admin/deletealldis", method = RequestMethod.GET)
	public String deleteDis(){
		distributorService.deleteAllDistributor();
		return "redirect:/admin";
	}
	
	@RequestMapping(value = "/admin/uploadsaledata", method = RequestMethod.POST)
	public String uploadSaleFile(@RequestParam("filesale") MultipartFile filesale,
			HttpServletRequest request) {
		List<SaleData> lsSale = new ArrayList<SaleData>();
		Set<String> sctSet = new HashSet<String>();
		Set<String> dupSet = new HashSet<String>();
		Set<String> dupName = new HashSet<String>();
		try {
			InputStream excelFile = filesale.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				int rowindex = currentRow.getRowNum();
				Iterator<Cell> cellIterator = currentRow.iterator();
				SaleData saleData = new SaleData();
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int colindex = currentCell.getColumnIndex();
					switch (colindex) {
					case 0:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							saleData.setDate(currentCell.getStringCellValue());
						}
						break;
					case 1:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							saleData.setSct(currentCell.getStringCellValue());
							sctSet.add(currentCell.getStringCellValue());
						}
						break;
					case 2:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							saleData.setName(currentCell.getStringCellValue());
						}
						break;
					case 3:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							saleData.setFactory(currentCell.getStringCellValue());
						}
						break;

					default:
						break;
					}
				}
				lsSale.add(saleData);
			}
			for (SaleData saledata : lsSale) {
				for (SaleData saleexist : lsSale) {
					if(saleexist.getDate().equalsIgnoreCase(saledata.getDate()) && saleexist.getName().equalsIgnoreCase(saledata.getName())){
						if(!saleexist.getFactory().equalsIgnoreCase(saledata.getFactory())){
							String dup = saledata.getDate() + " " + saledata.getName() + " "+ saledata.getFactory();
							dupSet.add(dup);
							dupName.add(saledata.getName());
						}
					}
				}
			}
			Iterator value = dupSet.iterator(); 
			while (value.hasNext()) { 
	            System.out.println(value.next()); 
	        } 
			System.out.println("sctSet: " + sctSet.size());
			System.out.println("dupSet: "+ dupSet.size());
			System.out.println("dupName: " + dupName.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin";
	}
	
	@RequestMapping(value = "/admin/uploadfile", method = RequestMethod.POST)
	public String uploadFile(@RequestParam("file") MultipartFile file,
			HttpServletRequest request) {
		List<User> listSale = new ArrayList<User>();
		List<Distributor> lsDistributor = new ArrayList<Distributor>();
		Set<String> areas = new HashSet<String>();
		try {
			String nameFile = file.getOriginalFilename();
			InputStream excelFile = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				int rowindex = currentRow.getRowNum();
				if(rowindex == 0){
					continue;
				}
				Distributor distributor = new Distributor();
				User user = new User();
				user.setIsAdmin(Constants.ROLE_SALE);
				Iterator<Cell> cellIterator = currentRow.iterator();
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int colindex = currentCell.getColumnIndex();
					switch (colindex) {
					case 0:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							distributor.setId(currentCell.getStringCellValue());
						}if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							distributor.setId(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 1:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							distributor.setFullname(currentCell.getStringCellValue());
						}if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							distributor.setFullname(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 2:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							distributor.setShortname(currentCell.getStringCellValue());
						}if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							distributor.setShortname(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 3:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							distributor.setArea(currentCell.getStringCellValue());
							areas.add(currentCell.getStringCellValue());
						}if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							distributor.setArea(String.valueOf(currentCell
									.getNumericCellValue()));
							areas.add(currentCell.getStringCellValue());
						}
						break;
					case 4:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							distributor.setProvince(currentCell.getStringCellValue());
						}if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							distributor.setProvince(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 5:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							distributor.setAddress(currentCell.getStringCellValue());
						}if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							distributor.setAddress(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 6:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							distributor.setPhone(currentCell.getStringCellValue());
						}if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							distributor.setPhone(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 7:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							distributor.setEmail(currentCell.getStringCellValue());
						}if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							distributor.setEmail(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 8:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							user.setUsername(currentCell.getStringCellValue());
							distributor.setSale(currentCell.getStringCellValue());
							user.setEmail(currentCell.getStringCellValue());
							user.setPassword("12345678");
						}
						break;
					case 9:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							user.setFullname(currentCell.getStringCellValue());
						}
						break;
					case 10:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							user.setShortname(currentCell.getStringCellValue());
						}
						break;
					case 11:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							user.setPhone(currentCell.getStringCellValue());
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							user.setPhone(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					default:
						break;
					}
				}
				listSale.add(user);
				lsDistributor.add(distributor);
			}
			for (User user : listSale) {
				String username = user.getUsername();
				User existUser = userService.findbyUsername(username);
				if(existUser == null){
					userService.saveUser(user);
				}
			}
			for (Distributor distributor : lsDistributor) {
				String id = distributor.getId();
				Distributor existDis = distributorService.findById(id);
				if(existDis == null){
					distributorService.saveDistributor(distributor);
				}else {
					existDis.setFullname(distributor.getFullname());
					existDis.setShortname(distributor.getShortname());
					existDis.setArea(distributor.getArea());
					existDis.setProvince(distributor.getProvince());
					existDis.setAddress(distributor.getAddress());
					existDis.setPhone(distributor.getPhone());
					existDis.setEmail(distributor.getEmail());
					existDis.setSale(distributor.getSale());
					distributorService.saveDistributor(existDis);
				}
			}
			for (String area : areas) {
				Area existArea = areaService.findByName(area);
				if(existArea == null){
					Area areaNew = new Area();
					areaNew.setName(area);
					areaService.saveArea(areaNew);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin";
	}
	
}
