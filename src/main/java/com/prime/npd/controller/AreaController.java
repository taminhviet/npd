package com.prime.npd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.prime.npd.model.Area;
import com.prime.npd.model.Factory;
import com.prime.npd.service.AreaService;

@Controller
public class AreaController {
	
	@Autowired
	AreaService areaService;

	@RequestMapping(value = "/admin/area", method = RequestMethod.GET)
	public String area(Model model) {
		List<Area> lsArea = areaService.findAllArea();
		if(lsArea != null && !lsArea.isEmpty()){
			model.addAttribute("lsArea", lsArea);
		}
		return "area";
	}
	
	@RequestMapping(value = "/admin/addarea", method = RequestMethod.POST)
	public String addArea(Model model,
			@RequestParam("name") String name){
		Area area = new Area();
		area.setName(name);
		areaService.saveArea(area);
		return "redirect:/admin/area";
	}
	
	@RequestMapping(value = "/admin/deleteallarea", method = RequestMethod.GET)
	public String deletearea(Model model) {
		areaService.deleteArea();
		return "redirect:/admin/area";
	}
}
