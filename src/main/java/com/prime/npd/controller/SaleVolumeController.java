package com.prime.npd.controller;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.prime.npd.model.SaleVolume;
import com.prime.npd.service.SaleVolumeService;
import com.prime.npd.util.Constants;

@Controller
public class SaleVolumeController {

	@Autowired
	SaleVolumeService saleVolumeService;
	
	@RequestMapping(value = "/admin/salevolume", method = RequestMethod.GET)
	public String salevolume(Model model) {
		List<SaleVolume> saleVolumes = saleVolumeService.findTop2k();
		if(saleVolumes != null && !saleVolumes.isEmpty()){
			model.addAttribute("saleVolumes", saleVolumes);
		}
		return "salevolume";
	}
	
	@RequestMapping(value = "/admin/deletesalevolume", method = RequestMethod.GET)
	public String deletesalevolume(Model model) {
		saleVolumeService.deleteAll();
		return "redirect:/admin/salevolume";
	}
	
	@RequestMapping(value = "/admin/uploadsalevolume", method = RequestMethod.POST)
	public String uploadsalevolume(@RequestParam("file") MultipartFile file,
			HttpServletRequest request) {
		List<SaleVolume> saleVolumes = new ArrayList<SaleVolume>();
		try {
			String nameFile = file.getOriginalFilename();
			InputStream excelFile = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			NumberFormat formatter = new DecimalFormat("#0");
			Calendar cal = Calendar.getInstance();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				int rowindex = currentRow.getRowNum();
				if(rowindex == 0){
					continue;
				}
				SaleVolume saleVolume = new SaleVolume();
				Iterator<Cell> cellIterator = currentRow.iterator();
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int colindex = currentCell.getColumnIndex();
					switch (colindex) {
					case 0:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							saleVolume.setOrderno(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = formatter.format(currentCell.getNumericCellValue());
							saleVolume.setOrderno(cellValue);
						}
						break;
					case 1:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							saleVolume.setPonumber(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = formatter.format(currentCell.getNumericCellValue());
							saleVolume.setPonumber(cellValue);
						}
						break;
					case 2:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							Date requestDate = df.parse(cellValue);
							saleVolume.setRequestdate(requestDate);
							cal.setTime(requestDate);
							int month = cal.get(Calendar.MONTH)+1;
							int year = cal.get(Calendar.YEAR);
							String monthStr = String.valueOf(month);
							if(month < 10){
								monthStr = "0".concat(monthStr);
							}
							String saleMonth = year + Constants.DOT + monthStr;
							saleVolume.setMonth(saleMonth);
						}
						break;
					case 3:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							saleVolume.setFullname(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf(currentCell.getNumericCellValue());
							saleVolume.setFullname(cellValue);
						}
						break;
					case 4:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							saleVolume.setOrderitem(cellValue);
						}
						break;
					case 5:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							saleVolume.setQuantities(Double.valueOf(cellValue));
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							double cellValue = currentCell.getNumericCellValue();
							saleVolume.setQuantities(cellValue);
						}
						break;
					case 6:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							saleVolume.setFactory(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf(currentCell.getNumericCellValue());
							saleVolume.setFactory(cellValue);
						}
						break;
					case 7:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							saleVolume.setSize(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf(currentCell.getNumericCellValue());
							saleVolume.setSize(cellValue);
						}
						break;
					case 8:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							saleVolume.setShortname(cellValue);
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							String cellValue = String.valueOf(currentCell.getNumericCellValue());
							saleVolume.setShortname(cellValue);
						}
						break;
					case 9:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							saleVolume.setSquaremeter(Double.valueOf(cellValue));
						}else if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
							double cellValue = currentCell.getNumericCellValue();
							saleVolume.setSquaremeter(cellValue);
						}
						break;
					case 10:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							saleVolume.setArea(cellValue);
						}
						break;
					default:
						break;
					}
				}
				if(saleVolume.getSquaremeter() > 0){
					saleVolumes.add(saleVolume);
				}
			}
			if(!saleVolumes.isEmpty()){
				for (SaleVolume saleVolume : saleVolumes) {
					List<SaleVolume> existSV = saleVolumeService.findByDisDate(saleVolume.getOrderitem(), saleVolume.getFullname(), saleVolume.getRequestdate(), saleVolume.getOrderno());
					if(existSV == null || existSV.isEmpty()){
						saleVolumeService.saveSaleVolume(saleVolume);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/salevolume";
	}
}
