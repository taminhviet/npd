package com.prime.npd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.prime.npd.model.User;
import com.prime.npd.service.UserService;
import com.prime.npd.util.Constants;

@Controller
public class UserController {
	
	@Autowired
	UserService userService;

	@RequestMapping(value = "/admin/user", method = RequestMethod.GET)
	public String user(Model model){
		List<User> lsUser = userService.findSale();
		if(lsUser != null && !lsUser.isEmpty()){
			model.addAttribute("lsUser", lsUser);
		}
		return "user";
	}
	
	@RequestMapping(value = "/admin/deleteuser", method = RequestMethod.POST)
	public String deleteuser(Model model,@RequestParam("username") String username){
		User user = userService.findbyUsername(username);
		userService.deleteUser(user);
		return "redirect:/admin/user";
	}
	
	@RequestMapping(value = "/admin/resetpw", method = RequestMethod.GET)
	public String resetpw(Model model){
		return "resetpw";
	}
	
	@RequestMapping(value = "/admin/resetpw", method = RequestMethod.POST)
	public String doresetpw(Model model, @RequestParam("username") String username,
			@RequestParam("newpw") String newpw) {
		User user = userService.findbyUsername(username);
		if(user != null) {
			user.setPassword(newpw);
			userService.saveUser(user);
		}
		return "resetpw";
	}
			
			
	@RequestMapping(value = "/admin/adduser", method = RequestMethod.POST)
	public String adduser(Model model,@RequestParam("username") String username,
			@RequestParam("password") String password,
			@RequestParam("fullname") String fullname,
			@RequestParam("shortname") String shortname,
			@RequestParam("address") String address,
			@RequestParam("email") String email,
			@RequestParam("phone") String phone){
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setFullname(fullname);
		user.setShortname(shortname);
		user.setAddress(address);
		user.setEmail(email);
		user.setPhone(phone);
		user.setIsAdmin(Constants.ROLE_SALE);
		userService.saveUser(user);
		return "redirect:/admin/user";
	}
}
