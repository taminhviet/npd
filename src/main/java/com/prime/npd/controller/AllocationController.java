package com.prime.npd.controller;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.prime.npd.model.Distributor;
import com.prime.npd.model.ObjectID;
import com.prime.npd.model.ProductAllocation;
import com.prime.npd.model.SaleVolume;
import com.prime.npd.service.DistributorService;
import com.prime.npd.service.ProductAllocationService;
import com.prime.npd.util.Constants;

@Controller
public class AllocationController {

	@Autowired
	DistributorService distributorService;
	
	@Autowired
	ProductAllocationService productAllocationService;
	
	@RequestMapping(value = "/admin/allocation", method = RequestMethod.GET)
	public String allocation(Model model){
		List<ProductAllocation> lsProductAllocation = productAllocationService.get5000Allo();
		if(lsProductAllocation != null && !lsProductAllocation.isEmpty()){
			model.addAttribute("lsProductAllocation", lsProductAllocation);
		}
		return "allocation";
	}
	
	@RequestMapping(value = "/admin/deleteallocation", method = RequestMethod.GET)
	public String deleteallocation(Model model){
		productAllocationService.deleteAll();
		return "redirect:/admin/allocation";
	}
	
	@RequestMapping(value = "/admin/deletepa", method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deletepa(Model model, @RequestBody String json){
		Gson gson = new Gson();
		String result = Constants.FAILED;
		ObjectID object = gson.fromJson(json, ObjectID.class);
		int id = object.getId();
		ProductAllocation pa = productAllocationService.findById(id);
		if(pa != null) {
			productAllocationService.deletePA(pa);
			result = Constants.SUCCESS;
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "admin/deletepamonth", method = RequestMethod.POST)
	public String deletepamonth(Model model, @RequestParam("month") String month) {
		List<ProductAllocation> padelete = productAllocationService.findByMonth(month);
		if(padelete != null && !padelete.isEmpty()) {
			for (ProductAllocation productAllocation : padelete) {
				productAllocationService.deletePA(productAllocation);
			}
		}
		return "redirect:/admin/allocation";
	}
	@RequestMapping(value = "admin/deletepacode", params = "delete", method = RequestMethod.POST)
	public String deletepacode(Model model, @RequestParam("code") String code) {
		List<ProductAllocation> padelete = productAllocationService.findByCode(code);
		if(padelete != null && !padelete.isEmpty()) {
			for (ProductAllocation productAllocation : padelete) {
				productAllocationService.deletePA(productAllocation);
			}
		}
		return "redirect:/admin/allocation";
	}
	@RequestMapping(value = "admin/deletepacode", params = "listall", method = RequestMethod.POST)
	public String listpacode(Model model, @RequestParam("code") String code) {
		List<ProductAllocation> listall = productAllocationService.findByCode(code);
		if(listall != null && !listall.isEmpty()) {
			model.addAttribute("lsProductAllocation", listall);
		}
		return "allocation";
	}
	
	@RequestMapping(value = "/admin/uploadallocation", method = RequestMethod.POST)
	public String uploadallocation(@RequestParam("file") MultipartFile file,
			HttpServletRequest request) {
		List<ProductAllocation> lsProductAllocation = new ArrayList<ProductAllocation>();
		List<Distributor> lsDistributor = distributorService.findAll();
		Map<String, String> maptaxid = new HashedMap<String, String>();
		//Map Shortname and TaxId
		for (Distributor distributor : lsDistributor) {
			String shortName = distributor.getShortname();
			String id = distributor.getId();
			maptaxid.put(shortName, id);
		}
		try {
			String nameFile = file.getOriginalFilename();
			InputStream excelFile = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			int totalSheet = workbook.getActiveSheetIndex();
			System.out.println("total sheet:" + totalSheet);
			for (int i = 0; i <= totalSheet; i++) {
				Sheet datatypeSheet = workbook.getSheetAt(i);
				Iterator<Row> iterator = datatypeSheet.iterator();
				Map<Integer, String> mapDis = new HashedMap<Integer, String>();
				while (iterator.hasNext()) {
					Row currentRow = iterator.next();
					int rowindex = currentRow.getRowNum();
					Iterator<Cell> cellIterator = currentRow.iterator();
					if(rowindex == 0){
						while (cellIterator.hasNext()) {
							Cell currentCell = cellIterator.next();
							int colindex = currentCell.getColumnIndex();
							if(colindex >= 8){
								String cellString = currentCell.toString();
								if(!cellString.contains("#")) {
									String cellValue = currentCell.getStringCellValue();
									mapDis.put(colindex, cellValue);
								}
							}
						}
						continue;
					}
					Cell cellCode = currentRow.getCell(0);
					String code = null;
					String month = null;
					if(cellCode != null) {
						code = cellCode.getStringCellValue();
						Cell cellMonth = currentRow.getCell(2);
						if (cellMonth.getCellTypeEnum() == CellType.STRING) {
							month= cellMonth.getStringCellValue();
						}else if (cellMonth.getCellTypeEnum() == CellType.NUMERIC){
							month = String.valueOf(cellMonth.getNumericCellValue());
						}
						if(month.contains(Constants.SINGLE_QUOTE)){
							month = month.replace(Constants.SINGLE_QUOTE, Constants.EMPTY);
						}
					}
					
					while (cellIterator.hasNext()) {
						Cell currentCell = cellIterator.next();
						int colindex = currentCell.getColumnIndex();
						if(colindex >= 8){
							if (currentCell.getCellTypeEnum() == CellType.STRING) {
								String cellValue = currentCell.getStringCellValue();
								if(cellValue != null && !cellValue.isEmpty()){
									if(code != null) {
										ProductAllocation pa = new ProductAllocation();
										pa.setCode(code);
										pa.setMonth(month);
										String shortname = mapDis.get(colindex);
										if(shortname != null){
											pa.setShortname(shortname.trim());
											String taxid = maptaxid.get(shortname);
											pa.setTaxid(taxid);
										}
										lsProductAllocation.add(pa);
									}
								}
							}
						}
					}
				}
			}
			
		for (ProductAllocation pa : lsProductAllocation) {
			ProductAllocation existPA = productAllocationService.searchByDis(pa.getCode(), pa.getTaxid());
			if(existPA == null){
				productAllocationService.saveProductAllocation(pa);
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/allocation";
	}
	
	@RequestMapping(value = "/admin/uploaddeletepa", method = RequestMethod.POST)
	public String uploaddeletepa(@RequestParam("file") MultipartFile file,
			HttpServletRequest request) {
		List<ProductAllocation> lsProductAllocation = new ArrayList<ProductAllocation>();
		List<Distributor> lsDistributor = distributorService.findAll();
		Map<String, String> maptaxid = new HashedMap<String, String>();
		//Map Shortname and TaxId
		for (Distributor distributor : lsDistributor) {
			String shortName = distributor.getShortname();
			String id = distributor.getId();
			maptaxid.put(shortName, id);
		}
		try {
			String nameFile = file.getOriginalFilename();
			InputStream excelFile = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			Map<Integer, String> mapDis = new HashedMap<Integer, String>();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				int rowindex = currentRow.getRowNum();
				Iterator<Cell> cellIterator = currentRow.iterator();
				if(rowindex == 0){
					while (cellIterator.hasNext()) {
						Cell currentCell = cellIterator.next();
						int colindex = currentCell.getColumnIndex();
						if(colindex >= 8){
							String cellValue = currentCell.getStringCellValue();
							mapDis.put(colindex, cellValue);
						}
					}
					continue;
				}
				Cell cellCode = currentRow.getCell(0);
				String code = cellCode.getStringCellValue();
				Cell cellMonth = currentRow.getCell(2);
				String month = null;
				if (cellMonth.getCellTypeEnum() == CellType.STRING) {
					month= cellMonth.getStringCellValue();
				}else if (cellMonth.getCellTypeEnum() == CellType.NUMERIC){
					month = String.valueOf(cellMonth.getNumericCellValue());
				}
				if(month.contains(Constants.SINGLE_QUOTE)){
					month = month.replace(Constants.SINGLE_QUOTE, Constants.EMPTY);
				}
				
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int colindex = currentCell.getColumnIndex();
					if(colindex >= 8){
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							String cellValue = currentCell.getStringCellValue();
							if(cellValue != null && !cellValue.isEmpty()){
								ProductAllocation pa = new ProductAllocation();
								pa.setCode(code);
								pa.setMonth(month);
								String shortname = mapDis.get(colindex);
								if(shortname != null){
									pa.setShortname(shortname);
									String taxid = maptaxid.get(shortname);
									pa.setTaxid(taxid);
								}
								lsProductAllocation.add(pa);
							}
						}
					}
				}
			}
		for (ProductAllocation pa : lsProductAllocation) {
			ProductAllocation existPA = productAllocationService.searchByDis(pa.getCode(), pa.getTaxid());
			if(existPA != null){
				productAllocationService.deletePA(existPA);
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/allocation";
	}
}
