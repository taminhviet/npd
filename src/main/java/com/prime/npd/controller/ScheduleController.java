package com.prime.npd.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.prime.npd.model.FirstSale;
import com.prime.npd.model.Product;
import com.prime.npd.model.SaleVolume;
import com.prime.npd.service.FirstSaleService;
import com.prime.npd.service.ProductService;
import com.prime.npd.service.SaleVolumeService;

@Component
public class ScheduleController {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	SaleVolumeService saleVolumeService;
	
	@Autowired
	FirstSaleService firstSaleService;
	
	@Scheduled(fixedRate = 10000000)
	public void findFirstSale() {
		//list all product
		List<Product> allProducts = productService.getAllProducts();
		for (Product product : allProducts) {
			String code = product.getCode();
			FirstSale firstSale = firstSaleService.findByCode(code);
			if(firstSale == null) {
				List<SaleVolume> saleVolumes = saleVolumeService.findByOrderitem(code);
				if(saleVolumes != null && !saleVolumes.isEmpty()){
					Collections.sort(saleVolumes, new Comparator<SaleVolume>() {
						
						@Override
						public int compare(SaleVolume o1, SaleVolume o2) {
							Date date1 = o1.getRequestdate();
							Date date2 = o2.getRequestdate();
							return date1.compareTo(date2);
						}
						
					});
					SaleVolume sv = saleVolumes.get(0);
					Date dateSale = sv.getRequestdate();
					FirstSale newSale = new FirstSale();
					newSale.setCode(code);
					newSale.setFirstDate(dateSale);
					firstSaleService.saveFirstSale(newSale);
				}
			}
		}
	}

}
