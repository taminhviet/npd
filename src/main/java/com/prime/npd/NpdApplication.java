package com.prime.npd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class NpdApplication {

	public static void main(String[] args) {
		SpringApplication.run(NpdApplication.class, args);
	}
}
