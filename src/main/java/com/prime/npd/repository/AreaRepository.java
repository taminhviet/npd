package com.prime.npd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.Area;

@Repository
public interface AreaRepository extends JpaRepository<Area, Integer>{

	Area findByName(String name);
}
