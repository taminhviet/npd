package com.prime.npd.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.ProductAllocation;

@Repository
public interface ProductAllocationRepository extends JpaRepository<ProductAllocation, Integer>{
	
	List<ProductAllocation> findByShortname(String shortname);
	
	List<ProductAllocation> findByMonth(String month);

	List<ProductAllocation> findByCode(String code);
	
	
	@Transactional
	@Query("SELECT p FROM ProductAllocation p WHERE p.code = ?1 AND p.taxid = ?2")
	List<ProductAllocation> searchByDis(String code, String taxid);
	
	@Transactional
	@Query("SELECT p FROM ProductAllocation p WHERE p.code = :code AND p.month LIKE CONCAT('%',:year,'%')")
	List<ProductAllocation> findAllocationWithYear(@Param("code") String code,@Param("year") String year);
	
	@Transactional
	@Query("SELECT p FROM ProductAllocation p WHERE p.shortname = ?1 AND p.month = ?2")
	List<ProductAllocation> searchByMonthName(String shortname, String month);
	
	@Transactional
	@Query("SELECT p FROM ProductAllocation p WHERE p.shortname = :shortname AND p.month LIKE CONCAT('%',:year,'%')")
	List<ProductAllocation> searchByYearName(@Param("shortname") String shortname,@Param("year") String year);
}
