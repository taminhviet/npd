package com.prime.npd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{

	Product findByCode(String code);
	List<Product> findBySize(String size);
	List<Product> findByFactory(String Factory);
}
