package com.prime.npd.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.ProductSize;
import com.prime.npd.model.SaleVolume;

@Repository
public interface SaleVolumeRepository extends JpaRepository<SaleVolume, Integer>{
	
	List<SaleVolume> findByFullname(String fullname);
	
	List<SaleVolume> findByShortname(String shortname);
	
	@Transactional
	@Query("SELECT s FROM SaleVolume s WHERE s.orderitem = ?1 AND s.fullname = ?2")
	List<SaleVolume> searchByDis(String orderitem, String fullname);
	
	@Transactional
	@Query("SELECT s FROM SaleVolume s WHERE s.orderitem = ?1 AND s.fullname = ?2 AND s.requestdate = ?3 AND s.orderno = ?4")
	List<SaleVolume> searchByDisDate(String orderitem, String fullname, Date requestdate, String orderno);
	
	@Transactional
	@Query("SELECT s FROM SaleVolume s WHERE s.orderitem = ?1 AND s.area = ?2")
	List<SaleVolume> searchByArea(String orderitem, String area);
	
	@Transactional
	@Query("SELECT s FROM SaleVolume s WHERE s.orderitem = :orderitem AND s.requestdate LIKE CONCAT('%',:year,'%')")
	List<SaleVolume> findSaleVolumeWithYear(@Param("orderitem") String orderitem,@Param("year") String year);
	
	@Transactional
	@Query("SELECT s FROM SaleVolume s WHERE s.fullname = :fullname AND s.requestdate LIKE CONCAT('%',:year,'%')")
	List<SaleVolume> findSaleVolumeWithNPPYear(@Param("fullname") String fullname,@Param("year") String year);
	@Transactional
	@Query("SELECT s FROM SaleVolume s WHERE s.shortname = :shortname AND s.requestdate LIKE CONCAT('%',:year,'%')")
	List<SaleVolume> findSaleVolumeWithSNNPPYear(@Param("shortname") String shortname,@Param("year") String year);
	
	List<SaleVolume> findByOrderitem(String orderitem);
	
}
