package com.prime.npd.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.Sample;

@Repository
public interface SampleRepository extends JpaRepository<Sample, Integer>{
	List<Sample> findByCustomer(String customer);
	List<Sample> findByItem(String item);
	@Transactional
	@Query("SELECT s FROM Sample s WHERE s.customer = ?1 AND s.item = ?2")
	List<Sample> searchByCI(String customer, String item);
	
	@Transactional
	@Query("SELECT s FROM Sample s WHERE s.item = :item AND s.date LIKE CONCAT('%',:year,'%')")
	List<Sample> findSampleWithYear(@Param("item") String item,@Param("year") String year);
}
