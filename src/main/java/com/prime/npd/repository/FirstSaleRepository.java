package com.prime.npd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.FirstSale;

@Repository
public interface FirstSaleRepository extends JpaRepository<FirstSale, Integer>{

	FirstSale findByCode(String code);
}
