package com.prime.npd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.DistributorChanged;

@Repository
public interface DistributorChangedRepository extends JpaRepository<DistributorChanged, Integer>{

	DistributorChanged findById(String id);
}
