package com.prime.npd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.AuthenLog;

@Repository
public interface AuthenLogRepository extends JpaRepository<AuthenLog, Integer>{

	List<AuthenLog> findByUsername(String username);
}
