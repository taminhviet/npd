package com.prime.npd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.Distributor;

@Repository
public interface DistributorRepository extends JpaRepository<Distributor, Integer>{
	
	List<Distributor> findBySale(String sale);
	List<Distributor> findByArea(String area);
	Distributor findById(String id);
	Distributor findByShortname(String shortname);
	Distributor findByFullname(String fullname);
}
