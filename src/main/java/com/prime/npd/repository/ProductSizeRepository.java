package com.prime.npd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.ProductSize;

@Repository
public interface ProductSizeRepository extends JpaRepository<ProductSize, Integer>{
	
	ProductSize findBySize(String size);
}
