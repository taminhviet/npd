package com.prime.npd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prime.npd.model.Factory;

@Repository
public interface FactoryRepository extends JpaRepository<Factory, Integer>{

	Factory findById(String id);
}
