jQuery(document).ready(function($) {
	$('.col-left, #menu').hide();
	$('.angle-down').click(function() {
		// $(this).parents('li').addClass('sub-open');
		if($(window).width() < 1279) {
			$(this).next().slideToggle().toggleClass('open');
			$(this).closest('li').toggleClass('active');
			return false;
		}
	});
	$('.menu-open').click(function() {
		if($(window).width() > 1279) {
			// $('.header .menu-list .ul_menu').toggle("slide", { direction: "right" }, 800);
			$('.header .menu').toggleClass('nav-open');
		}else {
			// $('.header .menu-list .ul_menu').toggle("slide", { direction: "left" }, 800);
			$('.header .menu').toggleClass('nav-open');
		}
	});
	var isfullPage = false;
	var hash = document.URL.substr(document.URL.indexOf('#')+1);
	console.log(hash);
	if(hash == "home/1") {
		if($("#start-section").lenght > 0) {
			$('html, body').animate({
	            scrollTop: $("#start-section").offset().top - 90
	        }, 800);
		}
	}else if(hash == "partners") {
		if($("#other-partners").lenght > 0) {
			$('html, body').animate({
	            scrollTop: $("#other-partners").offset().top - 90
	        }, 800);
		}
	}
	function initialization(){
		if(isfullPage === false) {
			isfullPage = true;
		    var myFullpage = new fullpage('#fullpage', {
				anchors: ['home', 'partners'],
				menu: '#menu',
				licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
				navigation: false,
				slidesNavigation: false,
				css3: true,
				controlArrows: false,
				afterRender: function(){
					if(hash == 'partners') {
						fullpage_api.moveTo('home', 1);
						setTimeout(function(){ 
							fullpage_api.moveTo('partners', 0);
						}, 3000);
					}
					if(hash == "home/1" || hash == "partners" ) {
						$('.section-start').hide(); 
						$('#start-section .content-page, .col-left').show();
						$('#other-partners').addClass('show');
					}
				},
				afterLoad: function(origin, destination, direction){
					//console.log(hash);
				},
				onSlideLeave: function(anchorLink, index, slideIndex, direction){
					//console.log(slideIndex['index']);
					//if($(window).width() > 768) {
						if(slideIndex['index'] > 0) {
							$('body').addClass('partners-section');
							$('.header').addClass('header-2');
							$('.header .menu-list .ul_menu').toggle("slide", { direction: "right" }, 800);
						}else {
							$('body').removeClass('partners-section');
							$('.header').removeClass('header-2');
							$('.header .menu-list .ul_menu').show("slide", { direction: "right" }, 800);
							$('.header .menu').removeClass('nav-open');
						}
					//}
					if(slideIndex['index'] == 1) {
						$('#menu').fadeIn(); 
						setTimeout(function(){ 
							$('.section-start').fadeOut(500); 
						}, 2000);
						setTimeout(function(){ 
							$('#start-section .content-page, .col-left').fadeIn(800); 
							$('#other-partners').addClass('show');
							$('#menu .home-section').addClass('show-label');
						}, 2500);
					}else if(slideIndex['index'] == 0){
						$('.section-start').fadeIn(); 
						$('#start-section .content-page, #menu, .col-left').fadeOut(); 
						$('#other-partners').removeClass('show');
						$('#menu .home-section').removeClass('show-label');
					}
				}
			});
			var myFullpage1 = new fullpage('.fullpage-section', {
				anchors: ['s1', 's2', 's3', 's4', 's5', 's6', 's7', 's8', 's9', 's10', 's11', 's12', 's13', 's14', 's15'],
				licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
				navigation: false,
				slidesNavigation: false,
				css3: true,
				controlArrows: false,
			});
		}
	}
	if ($(window).width() < 1280) {
		$('.header ul.ul_menu li ul.sub-menu').css('display','none');
		if(isfullPage === true){
			isfullPage = false;
			fullpage_api.destroy('all');
		}
	}else {
		initialization();
	}
	$("#home-section .content-section a.link").click(function (){
        $('html, body').animate({
            scrollTop: $("#start-section").offset().top - 90
        }, 800);
    });
	$(window).resize(function() {
		if ($(window).width() < 1280) {
			$('.header ul.ul_menu li ul.sub-menu').css('display','none');
			if(isfullPage === true){
				isfullPage = false;
				fullpage_api.destroy('all');
				$('.header .menu').removeClass('nav-open'); 
				$('.header ul.ul_menu').hide();
			}
		}else {
			initialization();
			$('.header ul.ul_menu li ul.sub-menu').css('display','');
		}
	});
});