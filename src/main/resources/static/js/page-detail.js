jQuery(document).ready(function ($) {
    if ($('body.page-detail').length != 0) {
        var gallery = $('.gallery a:not(.to-gallery)').simpleLightbox({
            captionPosition: 'outside',
            showCounter: false,
            heightRatio: .658078431372549,
            close: false,
            navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="43.544" height="43.544" viewBox="0 0 43.544 43.544"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M-8677.5-5203.205v-.505h-.507v-30.282h7.067v23.723h23.726v7.064Z" transform="translate(2457.056 9837.27) rotate(45)"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" width="43.544" height="43.544" viewBox="0 0 43.544 43.544"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M-8677.5-5203.205v-.505h-.507v-30.282h7.067v23.723h23.726v7.064Z" transform="translate(-2413.513 -9793.726) rotate(-135)"/></svg>']
        });

        var left = $('.detail-col-right').position().left;
        var right = $(window).width() - left - $('.detail-col-right').width();
        $('.detail-col-right .fullpage-section .background').css({'left': '-'+left+'px', 'right': '-'+right+'px'});
        function getMaxheight(eles) {
            return Math.max.apply(null, eles.map(function ()
            {
                return $(this).outerHeight();
            }).get());
        }
        function setPosition() {
            $('body').removeClass('low-height');
            $('body').removeClass('center-content');
            $('.section .wrap').css('height', 'auto');
            var maxHeight = getMaxheight($('.section .wrap'));
            if (maxHeight > $(window).height() - 170) {
                $('body').addClass('low-height');
                $('.section .wrap').css('height', 'auto');
                maxHeight = getMaxheight($('.section .wrap'));
            }
            $('.section .wrap').each(function () {
                $(this).outerHeight(maxHeight);
            })

            if (($(window).height() - maxHeight)/2 > 170) {
                $('body').addClass('center-content');
                $('a.back').css('top', $('.detail-col-left').offset().top - 17 - $('a.back').height());
            }
            $('.page-detail .col-left').css('bottom', $(window).height() - $('.detail-col-left').outerHeight() - $('.detail-col-left').offset().top + $('.page-detail .col-left').width()/2 - 4);
        }
        if ($(window).width() >= 1280) {
            setPosition();
            setTimeout(setPosition, 1000);
        } else {
            $('body').removeClass('low-height');
            $('body').removeClass('center-content');
            $('.section .wrap').css('height', 'auto');
            $('a.back').css('top', '');
        }
        $(window).resize(function () {
            if ($(window).width() >= 1280) {
                left = $('.detail-col-right').position().left;
                right = $(window).width() - left - $('.detail-col-right').width();
                $('.detail-col-right .fullpage-section .background').css({'left': '-'+left+'px', 'right': '-'+right+'px'});
                setPosition();
            } else {
                $('.page-detail .col-left').css('bottom', '');
                $('body').removeClass('low-height');
                $('body').removeClass('center-content');
                $('.section .wrap').css('height', 'auto');
                $('a.back').css('top', '');
            }
        })
    }
});