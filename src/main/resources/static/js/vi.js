jQuery(document).ready(function(){
	var tableunassign = $('#tableorder').DataTable({
		columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
		"pageLength": 25,
		"order": [[ 1, "desc" ]]
	});
	var tableordersup = $('#tableordersup').DataTable({
		columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
		"pageLength": 25,
		"order": [[ 1, "desc" ]]
	});
	$('#tableallorder').DataTable({
		"pageLength": 25,
		"order": [[ 0, "desc" ]]
	});
	$('#tableneworder').DataTable({
		"pageLength": 25,
		"order": [[ 0, "desc" ]]
	});
	$('#tablestore').DataTable({
		"pageLength": 25,
		"order": [[ 0, "desc" ]]
	});
	$('#tablepaypal').DataTable({
		"pageLength": 25,
		"order": [[ 0, "desc" ]]
	});
	var tableorderdispute = $('#tableorderdispute').DataTable({
		"pageLength": 25,
		"ordering": false
//		"order": [[ 0, "desc" ]]
	});
	var tablesupplier = $('#tablesupplier').DataTable({
		"pageLength": 25,
		"order": [[ 0, "desc" ]]
	});
	
	$('#tablereturn').DataTable({
		"pageLength": 25,
		"order": [[ 0, "desc" ]]
	});
	$('#click').click(function () {
        var ids = $.map(tableunassign.rows('.selected').data(), function (item) {
            return item[1]
        });
        if(ids.length == 0){
        	alert("No row is selected!");
        }else {
        	$("#hiddenrow").val(ids);
        }
    });
	$('#btnmulti').click(function () {
        var ids = $.map(tableordersup.rows('.selected').data(), function (item) {
            return item[1]
        });
        if(ids.length == 0){
        	alert("No row is selected!");
        	return;
        }if (ids.length > 10){
        	alert("Maximum is 10 rows!");
        	return;
        }else {
        	$("#hiddenrow").val(ids);
        }
    });
	$("#div_focus").focus();
	jQuery('.add-new').hide();
	jQuery('.btn-add-new').click(function () {
        jQuery('.add-new').show();
    });

    jQuery('.btn-close-new').click(function () {
        jQuery('.add-new').hide();
    });
    jQuery('.add-new-satellite').hide();
	jQuery('.btn-add-new-satellite').click(function () {
        jQuery('.add-new-satellite').show();
    });

    jQuery('.btn-close-new-satellite').click(function () {
        jQuery('.add-new-satellite').hide();
    });
    jQuery('.add-new-gallery').hide();
	jQuery('.btn-add-new-gallery').click(function () {
        jQuery('.add-new-gallery').show();
    });

    jQuery('.btn-close-new-gallery').click(function () {
        jQuery('.add-new-gallery').hide();
    });
    
    $(document).on('change','.up', function(){
	   	var names = [];
	   	var length = $(this).get(0).files.length;
		    for (var i = 0; i < $(this).get(0).files.length; ++i) {
		        names.push($(this).get(0).files[i].name);
		    }
		    // $("input[name=file]").val(names);
			if(length>2){
			  var fileName = names.join(', ');
			  $(this).closest('.form-group').find('.form-control').attr("value",length+" files selected");
			}
			else{
				$(this).closest('.form-group').find('.form-control').attr("value",names);
			}
   });
    $(".btn[data-target='#editPartnerModal']").click(function() {
		var $row = $(this).closest("tr");
		var $partnerid = $row.find(".id").text();
		var $partnername = $row.find(".name").text();
		$("#partneridedit").val($partnerid);
		$("#partnernameedit").val($partnername);
	});
    $(".btn[data-target='#showSatellite']").click(function() {
		var $row = $(this).closest("tr");
		var $partnerid = $row.find(".id").text();
		var $partnername = $row.find(".name").text();
		$("#ModalSatelliteLabel").val($partnername);
		$.ajax({
			type : "GET",
			contentType : "application/json",
			url : "/admin/satellite/" + $partnerid,
			dataType: 'text',
			success: function (data) {
				$('.modal-body-satellite').html(data);
			},
			error: function (e) {
			}
		});
	});
	 $('#tableordersup').on('click','.btnDeliver', function (e) {
    	var $row = $(this).closest("tr");
    	var $orderid = $row.find(".id").text();
    	var $trackingnumber= $row.find('.trackingnumber').val();
    	var $carrier= $row.find('.carrier').val();
    	var $supplierwoo= $row.find('.supplierwoo').val();
    	var json = {
    		"id" : $orderid,
    		"trackingnumber" : $trackingnumber,
    		"carrier" : $carrier,
    		"supplierwoo" : $supplierwoo
    	};
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			data : JSON.stringify(json),
			url : "/supplier/ajaxdeliver",
			dataType: 'text',
			success: function (data) {
//				$('.modal-body-satellite').html(data);
			},
			error: function (e) {
			}
		});
    });
	$('#tableordersup').on('click','.btnNote', function (e) {
    	var $row = $(this).closest("tr");
    	var $orderid = $row.find(".id").text();
    	var $note= $row.find('.note').val();
    	var $note2= $row.find(".note2").val();
    	var $note3= $row.find(".note3").val();
    	var json = {
    		"id" : $orderid,
    		"note" : $note,
    		"note2" : $note2,
    		"note3" : $note3
    	};
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			data : JSON.stringify(json),
			url : "/supplier/ajax",
			dataType: 'text',
			success: function (data) {
				$row.find(".result").html(data);
			},
			error: function (e) {
			}
		});
    });
	$('#tableorderdispute').on('click','.btnUpdatetrack', function (e) {
    	var $row = $(this).closest("tr");
    	var $orderid = $row.find(".id").text();
    	var $serviceevidence= $row.find('.serviceevidence').val();
    	var $trackevidence= $row.find('.trackevidence').val();
    	var $noteevidence= $row.find('.noteevidence').val();
    	var json = {
    		"id" : $orderid,
    		"service" : $serviceevidence,
    		"track" : $trackevidence,
    		"note" : $noteevidence
    	};
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			data : JSON.stringify(json),
			url : "/admin/ajaxupdatetrack",
			dataType: 'text',
			success: function (data) {
				$row.find(".resultuptrack").html(data);
			},
			error: function (e) {
			}
		});
    });
	$('#tableorderdispute').on('click','.btnRefund', function (e) {
    	var $row = $(this).closest("tr");
    	var json = $row.find(".id").text();
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			data : JSON.stringify(json),
			url : "/admin/ajaxrefunddispute",
			dataType: 'text',
			success: function (data) {
				$row.find(".resultrefund").html(data);
			},
			error: function (e) {
			}
		});
    });
    $(".fullstat").hide();
    $('input[type=radio][name=display]').change(function() {
    	if (this.value == 'image') {
            $(".onlyimage").show();
            $(".fullstat").hide();
        }else if (this.value == 'full') {
        	$(".fullstat").show();
        	$(".onlyimage").hide();
        }
    });
    
});
function retrieveOrder() {
    var url = '/supplier/note';

    $("#resultsBlock").load(url);
}