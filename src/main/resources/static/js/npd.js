jQuery(document).ready(function(){
	$('#tablefactory').DataTable({
		"pageLength": 25,
		"order": [[ 0, "desc" ]]
	});
	$('#tablearea').DataTable({
		"pageLength": 25,
		"order": [[ 0, "desc" ]]
	});
	$('#tableuser').DataTable({
		"pageLength": 25,
		"order": [[ 0, "desc" ]]
	});
	$('#tabledistributor').DataTable({
		"pageLength": 25,
		"order": [[ 0, "desc" ]]
	});
	$('#tablesalevolume').DataTable({
		"pageLength": 50,
		"order": [[ 2, "desc" ]]
	});
	$('#tablesample').DataTable({
		"pageLength": 50,
		"order": [[ 2, "desc" ]]
	});
	$('#tableallocate').DataTable({
		"pageLength": 50,
		"order": [[ 0, "desc" ]]
	});
	$('#tableallocatedis').DataTable({
		"language": {
		    "search": "Tìm trong bảng"
		 },
		"pageLength": 50,
		"order": [[ 2, "desc" ]]
	});
	$('#tableproduct').DataTable({
		"pageLength": 50,
		"order": [[ 0, "desc" ]]
	});
	$('#tableallocation').DataTable({
		"pageLength": 50,
		"order": [[ 1, "desc" ]]
	});
	
	$('#tablelog').DataTable({
		dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf'
        ],
        "pageLength": 50,
		"order": [[ 0, "desc" ]]
	});
	
	$('#tabledistributor').on('click','.btnEditDis', function (e) {
    	var $row = $(this).closest("tr");
    	var $editid = $row.find(".distributorid").text();
    	var $fullname = $row.find(".fullname").text();
    	var $shortname = $row.find(".shortname").text();
    	var $address = $row.find(".address").text();
    	var $email = $row.find(".email").text();
    	var $phone = $row.find(".phone").text();
    	var $sale = $row.find(".sale").text();
    	$("#editid").val($editid.trim());
    	$("#editfullname").val($fullname.trim());
    	$("#editshortname").val($shortname.trim());
    	$("#editaddress").val($address.trim());
    	$("#editemail").val($email.trim());
    	$("#editphone").val($phone.trim());
    	$("div.editsalediv select").val($sale);
    });
	
	$('#tableallocation').on('click','.btnDelete', function (e) {
    	var $row = $(this).closest("tr");
    	var $id = $row.find(".id").text();
    	var json = {
    		"id" : $id
    	};
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			data : JSON.stringify(json),
			url : "/admin/deletepa",
			dataType: 'text',
			success: function (data) {
				$row.find(".result").html(data);
			},
			error: function (e) {
			}
		});
	});
});